import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'app.ipp.ic4c',
  appName: 'iCAN4Consumers',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    SplashScreen: {
      launchShowDuration: 30000,
      launchAutoHide: true,
      backgroundColor: '#080B1A'
    },
    GoogleAuth: {
      sopes: [
        "profile",
        "email"
      ],
      serverClientId: "511599695525-ta1imjf08nee0oj3jj42d6hik3iqkevj.apps.googleusercontent.com",
      forceCodeForRefreshToken: true
    }
  },
  cordova: {
    preferences: {
      allowsInlineMediaPlayback: "true"
    }
  }
};

export default config;
