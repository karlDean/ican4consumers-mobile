import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionSVGComponent } from 'src/app/components/question-svg/question-svg.component';
import { UnreceivedSvgComponent } from 'src/app/components/unreceived-svg/unreceived-svg.component';
import { UnrecognizedSvgComponent } from 'src/app/components/unrecognized-svg/unrecognized-svg.component';
import { CustomHeaderComponent } from 'src/app/components/custom-header/custom-header.component';
import { CustomFooterComponent } from 'src/app/components/custom-footer/custom-footer.component';
import { SupportSvgComponent } from 'src/app/components/support-svg/support-svg.component';
import { CancellationSvgComponent } from 'src/app/components/cancellation-svg/cancellation-svg.component';
import { DescriptorSearchComponent } from 'src/app/components/descriptor-search/descriptor-search.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TxnPickerComponent } from 'src/app/components/txn-picker/txn-picker.component';
import { ThankyouSvgComponent } from 'src/app/components/thankyou-svg/thankyou-svg.component';
import { EmailerComponent } from 'src/app/components/emailer/emailer.component';
import { RefundHistoryComponent } from 'src/app/components/refund-history/refund-history.component';



@NgModule({
  declarations: [
    QuestionSVGComponent,
    UnreceivedSvgComponent,
    UnrecognizedSvgComponent,
    CustomHeaderComponent,
    CustomFooterComponent,
    SupportSvgComponent,
    CancellationSvgComponent,
    DescriptorSearchComponent,
    TxnPickerComponent,
    ThankyouSvgComponent,
    EmailerComponent,
    RefundHistoryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  exports: [
    QuestionSVGComponent,
    UnreceivedSvgComponent,
    UnrecognizedSvgComponent,
    CustomHeaderComponent,
    CustomFooterComponent,
    SupportSvgComponent,
    CancellationSvgComponent,
    DescriptorSearchComponent,
    TxnPickerComponent,
    ThankyouSvgComponent,
    EmailerComponent,
    RefundHistoryComponent
  ]
})
export class CustomComponentsModule { }
