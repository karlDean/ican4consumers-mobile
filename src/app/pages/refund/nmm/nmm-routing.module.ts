import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NmmPage } from './nmm.page';

const routes: Routes = [
  {
    path: '',
    component: NmmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NmmPageRoutingModule {}
