import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NmmPageRoutingModule } from './nmm-routing.module';

import { NmmPage } from './nmm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NmmPageRoutingModule
  ],
  declarations: [NmmPage]
})
export class NmmPageModule {}
