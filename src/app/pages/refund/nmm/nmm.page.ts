import { Component, OnInit, ViewChild } from '@angular/core';
import { RefundingService } from 'src/app/services/refunding.service';
import { UiService } from 'src/app/services/ui.service';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nmm',
  templateUrl: './nmm.page.html',
  styleUrls: ['./nmm.page.scss'],
})
export class NmmPage implements OnInit {

  @ViewChild('captchaImg') captchaImg;

  loginID = "";
  password = "";
  repassword = "";

  public nmm = {
    merch_info: "",
    merch_business: "",
    merch_website: "",
    merch_email: "",
    merch_phone: "",
    firstname: "",
    lastname: "",
    contact_email: "",
    contact_phone: "",
    first6: "",
    last4: "",
    time: "",
    amount: "",
    purchaser_email: "",
    refund_reason: "",
    product: ""
  }

  constructor(
    private refund: RefundingService,
    private ui: UiService,
    private nav: NavController,
    private router: Router
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;

    console.log(state);

    this.nmm.merch_info = state.merch_info;
  }

  ionViewDidEnter() {
  }

  validate() {
    
    // return false;
    let disabled = this.nmm.merch_info && (this.nmm.merch_business || this.nmm.merch_website) && (this.nmm.merch_email || this.nmm.merch_phone) && this.nmm.firstname && this.nmm.lastname && (this.nmm.contact_email || this.nmm.contact_phone) && this.nmm.first6 && this.nmm.last4 && this.nmm.first6.length == 6 && this.nmm.last4.length == 4 && this.nmm.time && this.nmm.amount && this.nmm.purchaser_email && this.nmm.refund_reason && this.nmm.product ? false : true;

    return disabled;
  }

  async submit() {
    (await this.ui.loader('Requesting refund, please wait...')).present();
    await this.refund.nmmRefund(this.nmm).then(e => {
      this.ui.dismissLoader();
      this.router.navigate(['refund/nmm-result'], { state: { refundString: e } });
    }).catch(async e => {
      this.ui.dismissLoader();
      (await this.ui.alert(e)).present();
    });
  }

  back() {
    this.nav.back();
  }

}
