import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {

  @ViewChild('ic4cVid') ic4cVid;
  vidMuted = true;
  sliderOptions = {
    autoplay: {
      delay: 5000
    }
  }

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.ic4cVid = this.ic4cVid.nativeElement ? this.ic4cVid.nativeElement : this.ic4cVid;
    this.ic4cVid.muted = true;
    this.ic4cVid.play();

  }

  unmuteVid() {
    this.ic4cVid.muted = false;
    this.vidMuted = false;
  }

  muteVid() {
    this.ic4cVid.muted = true;
    this.vidMuted = true;
  }

  refund() {
    this.router.navigate(['refund/request']);
  }

  support() {
    this.router.navigate(['refund/support'])
  }
  
  email() {
    this.router.navigate(['contact-us'], { state: { redirected: true } });
  }

}
