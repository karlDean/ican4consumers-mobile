import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { DescriptorSearchComponent } from 'src/app/components/descriptor-search/descriptor-search.component';
import { SearchingService } from 'src/app/services/searching.service';
import { RefundingService } from 'src/app/services/refunding.service';
import { Router } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {
  
  trackingDetails;
  descriptor;
  transaction;
  type;

  refundDetails: {
    descriptor: string,
    refund_reason: string,
    first6: string,
    last4: string,
    purchase_date: string,
    amount: string,
    email_address: string,
    purchase_name: string
  } = {
    descriptor: '',
    refund_reason: '',
    first6: '',
    last4: '',
    purchase_date: '',
    amount: '',
    email_address: '',
    purchase_name: ''
  }
  temp_email = '';

  constructor(
    private modal: ModalController,
    public search: SearchingService,
    private refund: RefundingService,
    private router: Router,
    private ui: UiService,
    private nav: NavController
  ) { }

  ngOnInit() {

    let state = this.router.getCurrentNavigation().extras.state;

    if (!state)
      return;

    this.trackingDetails = state.trackingDetails ? state.trackingDetails : null;
    this.descriptor = state.descriptor ? state.descriptor : null;
    this.transaction = state.transaction ? state.transaction : null;
    this.type = state.type ? state.type : null;

    this.refundDetails.descriptor = this.descriptor.descriptor_text;
    
    switch (this.type) {
      case 'unreceived':
        this.refundDetails.refund_reason = "I did not received the good or service purchased.";
        break;
      
      case 'unrecognized':
        this.refundDetails.refund_reason = "I did not authorize this charge.";
        break;
      
      case 'cancellation':
        this.refundDetails.refund_reason = "I no longer want to continue receiving the goods/service (Please cancel my membership).";
        break;
    
      default:
        break;
    }

    if (this.trackingDetails && !this.transaction && !this.trackingDetails.order_no) {
      this.refundDetails.amount = this.trackingDetails.amount;
      this.temp_email = this.trackingDetails.email_address;
      this.refundDetails.first6 = this.trackingDetails.first6;
      this.refundDetails.last4 = this.trackingDetails.last4;
      this.refundDetails.purchase_date = this.trackingDetails.time;
      this.refundDetails.purchase_name = this.trackingDetails.firstname + " " + this.trackingDetails.lastname;
    }

    if (this.transaction) {

      if (this.transaction.cc_bin) {
        this.refundDetails.first6 = this.transaction.cc_bin;
      } else if (this.transaction.first6) {
        this.refundDetails.first6 = this.transaction.first6;
      } else if (this.trackingDetails.first6) {
        this.refundDetails.first6 = this.trackingDetails.first6;
      }

      if (this.transaction.last4) {
        this.refundDetails.last4 = this.transaction.last4;
      } else if (this.trackingDetails.last4) {
        this.refundDetails.last4 = this.trackingDetails.last4;
      }

      if (this.transaction.action) {
        let date: any = this.transaction.action.date;
        date = `${date.substring(0, 4)}-${date.substring(4, 6)}-${date.substring(6, 8)}`;
        this.refundDetails.purchase_date = date;
      } else if (this.transaction.time) {
        this.refundDetails.purchase_date = this.transaction.time;
      } else if (this.trackingDetails.time) {
        this.refundDetails.purchase_date = this.trackingDetails.time;
      }

      if (this.transaction.action) {
        this.refundDetails.amount = this.transaction.action.amount;
      } else if (this.transaction.amount) {
        this.refundDetails.amount = this.transaction.amount;
      } else if (this.trackingDetails.amount) {
        this.refundDetails = this.trackingDetails.amount;
      }

      if (this.transaction.email) {
        this.temp_email = this.transaction.email
      } else if (this.trackingDetails.email_address) {
        this.temp_email = this.trackingDetails.email;
      }

      if (this.transaction.first_name && this.transaction.last_name) {
        this.refundDetails.purchase_name = `${this.transaction.first_name} ${this.transaction.last_name}`;
      } else if (this.transaction.firstname && this.transaction.lastname) {
        this.refundDetails.purchase_name = `${this.transaction.firstname} ${this.transaction.lastname}`;
      } else if (this.trackingDetails.firstname && this.trackingDetails.lastname) {
        this.refundDetails.purchase_name = `${this.trackingDetails.firstname} ${this.trackingDetails.lastname}`;
      }
    }
  }

  async requestRefund() {

    (await this.ui.loader('Requesting refund')).present();

    let result: any = await this.refund.refund({ ...this.refundDetails, order_no: this.trackingDetails && this.trackingDetails.order_no ? this.trackingDetails.order_no : '' });

    this.ui.dismissLoader();

    console.log(result);

    this.router.navigate(['refund/result'], {
      state: {
        refundString: result.responsecode == 200 ? `We've successfully contacted the merchant on your behalf! If you have any question or need assistance, please call us at +1 855-660-3214. Thank you for using iCAN4Consumers, the only  complete independent Credit Adjustment Network for Consumers!` : result.responsedetails.errorstr
      }
    });
  }

  async searchMerchant() {
    let modal = await this.modal.create({
      component: DescriptorSearchComponent
    });
    modal.present();

    let result = await modal.onDidDismiss();
    
    if (result.role == 'cancel' || !result.data)
      return;
    
    let data = result.data;

    this.refundDetails.descriptor = data.descriptor_text;
  }

  check_params() {
    
    for (let i in this.refundDetails) {
      if (!this.refundDetails[i]) {
        return true;
      }
    }

    if (this.refundDetails.first6.length != 6)
      return true;
    
    if (this.refundDetails.last4.length != 4)
      return true;
    
    if (!this.search.validateEmail(this.temp_email))
      return true;
    
    if (!this.search.validateEmail(this.refundDetails.email_address))
      return true;
    
    if (this.temp_email != this.refundDetails.email_address)
      return true;
  }

  back() {
    this.nav.back();
  }

}
