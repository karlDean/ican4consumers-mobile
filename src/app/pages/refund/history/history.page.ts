import { Component, OnInit, ViewChild } from '@angular/core';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { RefundHistoryComponent } from 'src/app/components/refund-history/refund-history.component';
import { RefundingService } from 'src/app/services/refunding.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  @ViewChild('historySlide') historySlide: HTMLIonSlidesElement;
  searchKey;
  loading = true;
  pendingRefund = [];
  processedRefund = [];

  private sortKey = "date";
  private order: "asc" | "desc" = "desc";

  constructor(
    private modal: ModalController,
    private refundService: RefundingService,
    private actionSheet: ActionSheetController
  ) { }

  ngOnInit() {
  }

  async ionViewDidEnter() {
    this.historySlide.lockSwipes(true);
    let history = await this.refundService.getRefundHistory({ searchKey: '' });
    this.pendingRefund = history.pendingRefund;
    this.processedRefund = history.processedRefund;
    console.log(history);
    this.loading = false;
  }

  switchSlide(e) {
    let value = e.detail.value;
    this.historySlide.lockSwipes(false);
    
    switch (value) {
      case 'pending':
        this.historySlide.slidePrev();
        break;
      
      case 'processed':
        this.historySlide.slideNext();
        break;
    
      default:
        break;
    }

    this.historySlide.lockSwipes(true);
  }

  isRefund(e) {
    
    return /REFUND/gi.test(e);

  }

  isCancellation(e) {
    
    return /CANCEL/gi.test(e);

  }

  parseDate(e) {
    
    let date = new Date(e);
    return date.toLocaleDateString('en-US', {day: 'numeric', 'month': 'short', year: 'numeric'});

  }

  async showRefundInfo(history) {

    // let refund = this.pendingRefund.filter(e => e.request_id == id)[0];
    // if (!refund)
    //   refund = this.processedRefund.filter(e => e.request_id == id)[0];

    (await this.modal.create({
      component: RefundHistoryComponent,
      cssClass: 'refundDetails',
      mode: 'ios',
      componentProps: {refund: history}
    })).present();
  }

  async searchHistory() {
    this.pendingRefund = [];
    this.processedRefund = [];
    this.loading = true;
    let history = await this.refundService.getRefundHistory({ searchKey: this.searchKey });
    this.pendingRefund = history.pendingRefund;
    this.processedRefund = history.processedRefund;
    this.loading = false;
  }

  async sort() {

    let sort = await this.actionSheet.create({
      header: "Sort by",
      buttons: [
        {
          text: "Amount",
          role: "amount",
          icon: "swap-vertical"
        },
        {
          text: "Date Requested",
          role: "date",
          icon: "swap-vertical"
        },
        {
          text: "Email Address",
          role: "email",
          icon: "swap-vertical"
        },
      ]
    });

    sort.present();

    let sortVal: "amount" | "date" | "email" = (await sort.onDidDismiss()).role as "amount" | "date" | "email";

    if (!sortVal || sortVal as any == "backdrop")
      return;
    
    this.pendingRefund = [];
    this.processedRefund = [];
    
    if (sortVal == this.sortKey) {
      switch (this.order) {
        case 'asc':
          this.order = 'desc';
          break;
        
        case 'desc':
          this.order = 'asc';
          break;
      
        default:
          break;
      }
    }

    this.sortKey = sortVal;

    this.loading = true;
    let history = await this.refundService.getRefundHistory({ searchKey: this.searchKey, sort: sortVal, order: this.order});
    this.pendingRefund = history.pendingRefund;
    this.processedRefund = history.processedRefund;
    this.loading = false;
  }

}
