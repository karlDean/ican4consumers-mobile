import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  refund_text_result;
  @ViewChild('refundTextResult') resultstr;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    this.refund_text_result = state.refundString.replace(/[\[\]]/g, '');
  }

  ionViewWillEnter() {
    let p: HTMLParagraphElement = this.resultstr.nativeElement
    p.innerHTML = this.refund_text_result;
  }

  done() {
    this.router.navigate(['thank-you']);
  }

}