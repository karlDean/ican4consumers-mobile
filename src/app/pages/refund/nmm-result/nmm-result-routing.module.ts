import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NmmResultPage } from './nmm-result.page';

const routes: Routes = [
  {
    path: '',
    component: NmmResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NmmResultPageRoutingModule {}
