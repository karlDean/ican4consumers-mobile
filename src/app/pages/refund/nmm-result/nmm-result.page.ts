import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nmm-result',
  templateUrl: './nmm-result.page.html',
  styleUrls: ['./nmm-result.page.scss'],
})
export class NmmResultPage implements OnInit {

  refund_text_result;
  @ViewChild('refundTextResult') resultstr;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    let result_string = state.refundString.replace(/[\[\]]/g, '');
    result_string = decodeURIComponent(result_string);
    this.refund_text_result = result_string;
  }

  ionViewWillEnter() {
    let p: HTMLParagraphElement = this.resultstr.nativeElement
    p.innerHTML = this.refund_text_result;
  }

  done() {
    this.router.navigate(['thank-you']);
  }

}
