import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NmmResultPageRoutingModule } from './nmm-result-routing.module';

import { NmmResultPage } from './nmm-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NmmResultPageRoutingModule
  ],
  declarations: [NmmResultPage]
})
export class NmmResultPageModule {}
