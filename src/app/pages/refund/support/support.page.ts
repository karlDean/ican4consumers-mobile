import { Component, OnInit, ViewChild } from '@angular/core';
import { gsap } from 'gsap';
import { Router } from '@angular/router';
import { Browser } from '@capacitor/browser';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {

  @ViewChild('supportContent') supportContent;
  supportSearchKey;
  supQuest: [
    {
      parent: any,
      question: string
    }?
  ] = [];

  constructor(
    private router: Router,
    private nav: NavController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    
    let quest = document.querySelectorAll('.question');

    for (let i in quest) {
      if (typeof quest[i] != 'object')
        continue;
      
      let parent = quest[i].closest('.questions-item');
      let question = quest[i].textContent;
      this.supQuest.push({ question: question, parent: parent });
    }

    console.log(this.supQuest);

  }

  toggle(e: HTMLElement) {
    if (this.isVisible(e)) {

      console.log('visible');

      e.className = e.className.replace(/ visible/g, '');

    } else {

      console.log('divisible');

      e.className += ' visible';
    }
  }

  isVisible(e: HTMLElement) {
    let classname: string = e.className;
    if (classname.includes('visible')) {
      return true;
    } else {
      return false
    }
  }

  refund() {
    this.router.navigate(['refund/request']);
  }

  playstore() {
    window.location.href = 'https://play.google.com/store/apps/details?id=app.ipp.ic4c&hl=en';
  }

  appstore() {
    window.location.href = 'https://itunes.apple.com/us/app/ican4consumers/id1061524349?ls=1&mt=8';
  }

  fb() {
    window.location.href = 'https://www.facebook.com/ican4consumers';
  }

  async pci() {
    await Browser.open({
      url: 'https://www.pcisecuritystandards.org/',
      presentationStyle: "popover",
      toolbarColor: '#4279FC'
    })
  }

  termsAndConditions() {
    this.router.navigate(['terms-and-condition'], {state: {redirected: true}});
  }

  privacyPolicy() {
    this.router.navigate(['privacy-policy'], {state: {redirected: true}});
  }

  searchQuestion() {
    
    let regex = new RegExp(this.supportSearchKey, 'gi');

    for (let i of this.supQuest) {
      i.parent.style.display = 'block';
      let q = i.parent.querySelector('.question');
      q.textContent = i.question;
    }

    if (!this.supportSearchKey) {
      return;
    }
    
    for (let i of this.supQuest) {
      
      if (!regex.test(i.question)) {
        i.parent.style.display = 'none';
      }
      else {
        let matches = i.question.match(regex);
        let uniqueMatches = matches.filter((a, b, c) => c.indexOf(a) === b);
        
        for (let key of uniqueMatches) {
          let highlight = i.question.replace(new RegExp(key, 'g'), `<span style="background-color: #4279FC33;">${key}</span>`);
          let q = i.parent.querySelector('.question');
          q.innerHTML = highlight;
        }
      }
    }

  }

  back() {
    this.nav.back();
  }

}
