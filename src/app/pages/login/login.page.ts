import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { LoadingController } from '@ionic/angular';
import { UiService } from 'src/app/services/ui.service';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';
import { Keyboard } from '@capacitor/keyboard';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  showPassword = false;
  loggingIn = false;
  creds = {
    username: '',
    password: ''
  }

  constructor(
    private account: AccountService,
    private ui: UiService,
    private storage: StorageService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.creds.username = '';
    this.creds.password = '';
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }

  async login() {

    if (!this.creds.username || !this.creds.password)
      return;

    this.loggingIn = true;
    const login: any = await this.account.login(this.creds);

    if (login.responsecode != 200)
      (await this.ui.toast(login.responsemsg)).present();
    else {
      await this.storage.set({ user_info: login.responsedetails });

      let result: any = await this.account.getOtherEmails();
      
      await this.storage.set({ other_emails: result });

      this.account.accountChanged.next(login.responsedetails);

      this.router.navigate(['home']);
    }
    
    this.loggingIn = false;
  }

  async googleLogin() {

    (await this.ui.loader()).present();

    const login: any = await this.account.googleLogin().catch(async e => {

      (await this.ui.toast('Login cancelled.')).present();

      this.ui.dismissLoader();

    });

    if (!login || login.responsecode != 200)
      
      (await this.ui.toast(login && login.responsemsg ? login.responsemsg : "This Google account is not yet associated.")).present();

    else {

      await this.storage.set({ user_info: login.responsedetails });

      let result: any = await this.account.getOtherEmails();
      
      await this.storage.set({ other_emails: result });

      this.account.accountChanged.next(login.responsedetails);

      this.router.navigate(['home']);

    }

    this.ui.dismissLoader();
  }

  signUp() {
    this.router.navigate(['sign-up']);
  }

  next(e, input = null) {

    if (e.key != 'Enter')
      return;
    
    if (input)
      input.setFocus();
    else
      Keyboard.hide();
    
  }

  async loginAsGuest() {
    await this.storage.set({ "user_info": "guest" });
    this.account.accountChanged.next("guest");
    this.router.navigate(['/home']);
  }

}
