import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ProductQuestionAnimation } from 'src/app/animations/product-question-animation';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-product-question',
  templateUrl: './product-question.page.html',
  styleUrls: ['./product-question.page.scss'],
})
export class ProductQuestionPage implements OnInit {

  @ViewChild('mailto') mailto;

  public descriptor = null;
  public type;
  private anim = new ProductQuestionAnimation;

  constructor(
    private router: Router,
    private nav: NavController,
    private ui: UiService
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    this.descriptor = state.descriptor;
    this.type = state.type;
  }

  ionViewWillEnter() {
    this.anim.entrance();
  }

  back() {
    this.nav.back();
  }

  call() {
    if (this.descriptor && this.descriptor.contact_number)
      this.ui.dial(this.descriptor.contact_number.replace(/[^0-9]/g, ''));
    else
      this.ui.dial("8556603124");
  }

  chat() {
    this.router.navigate(['chatroom']);
  }

  email() {
    if (!this.descriptor || !this.descriptor.contact_email)
      this.router.navigate(['contact-us'], { state: { autoOpen: true, redirected: true } });
    else {
      let a = <HTMLAnchorElement>this.mailto.nativeElement;
      a.click();
    }
  }

}
