import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductQuestionPage } from './product-question.page';

const routes: Routes = [
  {
    path: '',
    component: ProductQuestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductQuestionPageRoutingModule {}
