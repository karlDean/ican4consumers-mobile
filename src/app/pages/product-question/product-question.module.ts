import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductQuestionPageRoutingModule } from './product-question-routing.module';

import { ProductQuestionPage } from './product-question.page';
import { CustomComponentsModule } from 'src/app/modules/custom-components/custom-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductQuestionPageRoutingModule,
    CustomComponentsModule
  ],
  declarations: [ProductQuestionPage]
})
export class ProductQuestionPageModule {}
