import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { Home_Animation } from 'src/app/animations/home_animation';
import { ModalController, AlertController } from '@ionic/angular';
import { DescriptorSearchComponent } from 'src/app/components/descriptor-search/descriptor-search.component';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account.service';
import { UiService } from 'src/app/services/ui.service';
import { SearchingService } from 'src/app/services/searching.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild('homeSlide') homeSlide: HTMLIonSlidesElement;
  user_info: any = {};
  other_emails: [];
  darkModeToggle: boolean;

  slideOpts = {
    autoHeight: true
  }

  anim = new Home_Animation;

  constructor(
    private storage: StorageService,
    private modal: ModalController,
    private router: Router,
    private account: AccountService,
    private alert: AlertController,
    private ui: UiService,
    private search: SearchingService

  ) { }

  ngOnInit() {
    
  }

  async ionViewWillEnter() {
    this.homeSlide.lockSwipes(true);
    this.storage.get('user_info').then(e => {
      this.user_info = e;
    })
    this.storage.get('other_emails').then((e: any) => {
      this.other_emails = e;
    });

    this.anim.header_footer_entrance();
    this.anim.main();

    let darkMode = this.ui.darkModeOn;

    if (darkMode == false || darkMode == true)
      this.darkModeToggle = darkMode;
    else
      this.darkModeToggle = false;
  }

  async question(searchRequired = false) {

    let result: any = await this.searchDescriptor();

    if (searchRequired) {

      if (result.role != 'select')
        return;
      
      this.router.navigate(['product-question'], {state: {descriptor: result.data, type: 2}});
      
    } else {

      if (result.role == "nmm")
        return;

      this.router.navigate(['product-question'], {state: {descriptor: result.data, type: 1}});

    }
  }

  async unreceived_order() {

    let result: any = await this.searchDescriptor();

    if (result.role != 'select')
      return;

    this.router.navigate(['track-order'], { state: { descriptor: result.data, type: 'unreceived' } });
  }

  async unrecognized_order() {

    let result: any = await this.searchDescriptor();

    if (result.role != 'select')
      return;

    this.router.navigate(['track-order'], { state: { descriptor: result.data, type: 'unrecognized' } });
  }

  async cancel_membership() {
    
    let result: any = await this.searchDescriptor();

    if (result.role != 'select')
      return;
    
      this.router.navigate(['track-order'], { state: { descriptor: result.data, type: 'cancellation' } });
  }

  async logout() {

    let alert = await this.alert.create({
      header: 'Logout',
      message: "Are you sure you want to logout?",
      buttons: [
        {
          text: 'Yes',
          role: 'logout'
        },
        {
          text: "No",
          role: 'cancel'
        }
      ]
    })
    alert.present();

    let result = await alert.onDidDismiss();

    if (result.role != 'logout')
      return;
    
    this.homeSlide.lockSwipes(false);
    this.homeSlide.slideTo(0);
    this.homeSlide.lockSwipes(true);

    setTimeout(async () => {  
      (await this.ui.loader('Logging out')).present();
      await this.account.logout();
      this.ui.dismissLoader();
      this.router.navigate(['login'], {replaceUrl: true});
    }, 1000);

  }

  async changePassword() {
    this.router.navigate(['change-password']);
  }

  async searchDescriptor() {
    const descriptor_search = await this.modal.create({
      component: DescriptorSearchComponent,
      swipeToClose: true
    })
    descriptor_search.present();

    let result = await descriptor_search.onDidDismiss();
    return new Promise(resolve => resolve(result));
  }

  async addEmail() {
    let alert = await this.alert.create({
      header: 'Add other email',
      inputs: [
        {
          name: "email",
          placeholder: "Email",
          type: "email"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          role: 'add'
        }
      ]
    });
    alert.present();

    let result = await alert.onDidDismiss();
    
    if (result.role != 'add')
      return;
    
    let email = result.data.values.email;

    if (!this.search.validateEmail(email)) {
      (await (this.ui.alert('Invalid email.'))).present();
      return;
    }

    let new_emails = this.other_emails.map((e: any) => e.emailaddress);
    new_emails = [
      ...new_emails,
      email
    ];

    this.postEmail(new_emails);
  }

  async removeEmail(email_address) {

    let alert = await this.alert.create({
      header: "Remove Email",
      message: "Are you sure you want to remove this email?",
      buttons: [
        {
          text: "Yes",
          role: 'remove'
        }, {
          text: "No",
          role: "cancel"
        }
      ]
    });
    alert.present();

    let result = await alert.onDidDismiss();

    if (result.role != 'remove')
      return;
    
    let new_emails: any = this.other_emails.filter((e: any) => e.emailaddress != email_address);
    new_emails = new_emails.map((e: any) => e.emailaddress);
    this.postEmail(new_emails);
  }

  async postEmail(new_emails: Array<string>) {
    (await this.ui.loader('Modifying email list')).present();
    
    let reqres = await this.account.modifyOtherEmails(new_emails);

    console.log(reqres)
    
    if (reqres.responsecode != 200) {
      this.ui.dismissLoader();
      (await this.ui.alert('Failed to add email.')).present();
      return;
    }

    let other_emails: any = await this.account.getOtherEmails();

    this.storage.set({ other_emails: other_emails });
    this.other_emails = other_emails;

    this.ui.dismissLoader();
  }

  toggleDarkMode() {

    // mode is from dark to light
    if (this.darkModeToggle) {
      this.ui.enableDarkMode(false);
    }
    // mode is from light to dark
    else {
      this.ui.enableDarkMode(true);
    }
    
  }

}
