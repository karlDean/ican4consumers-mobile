import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RefundingService } from 'src/app/services/refunding.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-cancellation-result',
  templateUrl: './cancellation-result.page.html',
  styleUrls: ['./cancellation-result.page.scss'],
})
export class CancellationResultPage implements OnInit {

  cancellation_text_result = null;
  descriptor;
  trackingDetails;

  constructor(
    private router: Router,
    private refund: RefundingService,
    private ui: UiService
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    this.descriptor = state.descriptor;
    this.trackingDetails = state.trackingDetails;
  }

  async ionViewDidEnter() {
    
    let params = {
      descriptor: this.descriptor.descriptor_text,
      firstname: this.trackingDetails.firstname,
      lastname: this.trackingDetails.lastname,
      email_address: this.trackingDetails.email_address,
      first6: this.trackingDetails.first6,
      last4: this.trackingDetails.last4,
      order_no: this.trackingDetails.order_no
    }

    let result: any = await this.refund.cancelMembership(params);
    
    console.log(result);

    if (!result || result.responsecode != 200) {
      (await this.ui.alert('An error occured. Please try again later.')).present();
      this.router.navigate(['thank-you']);
      return;
    }

    this.cancellation_text_result = result.responsemsg;
    
  }

  done() {
    this.router.navigate(['thank-you']);
  }

}
