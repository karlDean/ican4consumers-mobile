import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CancellationResultPage } from './cancellation-result.page';

const routes: Routes = [
  {
    path: '',
    component: CancellationResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CancellationResultPageRoutingModule {}
