import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CancellationResultPageRoutingModule } from './cancellation-result-routing.module';

import { CancellationResultPage } from './cancellation-result.page';
import { CustomComponentsModule } from 'src/app/modules/custom-components/custom-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CancellationResultPageRoutingModule,
    CustomComponentsModule
  ],
  declarations: [CancellationResultPage]
})
export class CancellationResultPageModule {}
