import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrackResultPageRoutingModule } from './track-result-routing.module';

import { TrackResultPage } from './track-result.page';
import { CustomComponentsModule } from 'src/app/modules/custom-components/custom-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrackResultPageRoutingModule,
    CustomComponentsModule
  ],
  declarations: [TrackResultPage]
})
export class TrackResultPageModule {}
