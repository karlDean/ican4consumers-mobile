import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchingService } from 'src/app/services/searching.service';
import { UiService } from 'src/app/services/ui.service';
import { ModalController, AlertController, NavController } from '@ionic/angular';
import { TxnPickerComponent } from 'src/app/components/txn-picker/txn-picker.component';
import { RefundingService } from 'src/app/services/refunding.service';

@Component({
  selector: 'app-track-result',
  templateUrl: './track-result.page.html',
  styleUrls: ['./track-result.page.scss'],
})
export class TrackResultPage implements OnInit {

  type = "unreceived";
  private descriptor: any = {};
  private trackingDetails = {};
  searching = true;
  selected_txn;

  constructor(
    private router: Router,
    private search: SearchingService,
    private refundSrv: RefundingService,
    private ui: UiService,
    private modal: ModalController,
    private alert: AlertController,
    private nav: NavController
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    this.descriptor = state.descriptor;
    this.trackingDetails = state.trackingDetails;
    this.type = state.type;
  }

  async ionViewDidEnter() {
    
    let transaction: any;

    if (this.descriptor.process_type == 'EMAIL-BASED') {
      this.emailBasedRefund();
      return;      
    } else if (this.type == 'cancellation') {
      transaction = await this.search.membership(this.trackingDetails, this.descriptor, this.type);
    } else {
      transaction = await this.search.transaction(this.trackingDetails, this.descriptor, this.type);
    }

    if (!transaction)
      return;
    
    if (transaction.responsecode != 200) {
      let alert = await this.alert.create({
        message: transaction.responsemsg,
        backdropDismiss: false,
        buttons: [
          {
            text: 'Back',
            role: 'back'
          },
          {
            text: 'OK',
            role: 'ok'
          }
        ]
      });
      alert.present();

      let role = (await alert.onDidDismiss()).role;

      if (role == "back") {

        this.nav.back();
        
      } else if (role == "ok") {
        
        this.router.navigate(['home'])

      }
      
      return;
    }

    let txn_list: any = transaction.responsedetails;

    if (!txn_list) {

      let alert = await this.alert.create({
        message: transaction.responsemsg,
        backdropDismiss: false,
        buttons: [
          {
            text: 'Back',
            role: 'back'
          },
          {
            text: 'OK',
            role: 'ok'
          }
        ]
      });
      alert.present();

      let role = (await alert.onDidDismiss()).role;

      if (role == "back") {

        this.nav.back();
        
      } else if (role == "ok") {
        
        this.router.navigate(['home'])

      }

      // (await this.ui.alert(transaction.responsemsg)).present();
      // this.ui.alertDidDismiss().then(e => this.router.navigate(['home']));
      return;
    }

    if (Array.isArray(txn_list)) {
      
      if (txn_list.length == 1) {

        this.selected_txn = txn_list[0];

      } else {

        let txn_picker = await this.modal.create({
          component: TxnPickerComponent,
          componentProps: {
            txns: txn_list,
            api_type: this.PROC_API()
          },
          animated: true,
          cssClass: "txn-picker-modal",
          showBackdrop: false
        });
        txn_picker.present();
        let result = await txn_picker.onDidDismiss();
        this.selected_txn = result.data;

      }

    } else {

      this.selected_txn = txn_list;

    }

    this.searching = false;

  }

  back() {
    this.nav.back();
    // this.router.navigate(['home']);
  }

  PROC_API() {
    
    const apis = ["API", "API2", "API3"];

    if (apis.includes(this.descriptor.process_type.toUpperCase()))
      return true;
    return false;

  }

  parseDate(time) {
    let date: any = new Date(time);
    let dateString = date.toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' });

    console.log('first datestring of ' + time + ' = ' + dateString);
    
    if (dateString != "Invalid Date") {
      return dateString;
    }
    
    date = time.substring(0, 8);
    date = new Date(`${date.substring(0, 4)}-${date.substring(4, 6)}-${date.substring(6, 8)}`);
    dateString = date.toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' });

    console.log('second datestring of ' + time + ' = ' + dateString);

    return dateString;
  }

  done() {
    this.router.navigate(['thank-you']);
  }

  cancelMembership() {
    
    this.router.navigate(['cancellation-result'], { state: { descriptor: this.descriptor, trackingDetails: this.trackingDetails } });

  }

  refund() {
    this.router.navigate(['refund/request'], {
      state: {
        trackingDetails: this.trackingDetails,
        descriptor: this.descriptor,
        transaction: this.selected_txn,
        type: this.type
      }
    });
  }

  async emailBasedRefund() {
    console.log(this.trackingDetails);
    console.log(this.descriptor);
    console.log(this.type);

    let refund_reason: string;

    switch (this.type) {
      case 'unreceived':
        refund_reason = "I didn't received what I ordered";
        break;
      case 'unrecognized':
        refund_reason = "I don't recognize this purchase.";
        break;
      case 'cancellation':
        refund_reason = "I want to cancel my membership";
        break;
    
      default:
        break;
    }

    let result: any = await this.refundSrv.refund({
      descriptor: this.descriptor.descriptor_text,
      amount: this.trackingDetails['amount'],
      email_address: this.trackingDetails['email_address'],
      first6: this.trackingDetails['first6'],
      last4: this.trackingDetails['last4'],
      purchase_date: this.trackingDetails['time'],
      purchase_name: this.trackingDetails['firstname'] + " " + this.trackingDetails['lastname'],
      refund_reason: refund_reason
    });

    console.log(result);

    if (result.responsecode == -1) {
      (await this.alert.create({
        header: "Refund Request Failed",
        message: "There is an existing refund request in our system with these details. Please contact your merchant for further assistance.  ",
        backdropDismiss: false,
        buttons: [
          {
            text: "Ok",
            handler: () => {
              this.router.navigate(['thank-you']);
            }
          }
        ]
      })).present();
      return;
    }

    var code = decodeURIComponent(result.responsedetails).split("<br/>")[2].replace(/[A-Za-z',.]/g, '').trim();

    (await this.alert.create({
      header: "Refund Requested",
      message: "We've successfully processed your refund request!\r\n\r\nYour confirmation code is "+code+".\r\n\r\nIf you have any question or need assistance, please call us at +1 855-660-3214.\r\nThank you for using iCAN4Consumers, the only completely independent Credit Adjustment Network for Consumers.",
      backdropDismiss: false,
      buttons: [
        {
          text: "Ok",
          handler: () => {
            this.router.navigate(['thank-you']);
          }
        }
      ]
    })).present();

  }

}
