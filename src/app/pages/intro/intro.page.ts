import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Slides } from 'src/app/animations/slides';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  @ViewChild('introSlide') slide: HTMLIonSlidesElement;
  @ViewChild('slide1') slide1: HTMLIonSlideElement;
  slideIndex = 1;
  private animation = new Slides;

  constructor(
    private router: Router,
    private storage: StorageService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.slide.lockSwipes(true);
    this.animation.slideAnim(this.slide1);
  }

  async next(slide) {
    this.slide.lockSwipes(false);
    this.slide.slideNext();
    this.slide.lockSwipes(true);
    this.slideIndex = await this.slide.getActiveIndex() + 1;
    if (this.slideIndex != 4)
      this.animation.slideAnim(slide);
    else
      this.animation.slideLastAnim(slide);
  }

  async back() {
    this.slide.lockSwipes(false);
    this.slide.slidePrev();
    this.slide.lockSwipes(true);
    this.slideIndex = await this.slide.getActiveIndex() + 1;
  }

  async login() {
    await this.storage.set({ intro: true });
    this.router.navigate(['home']);
  }

}
