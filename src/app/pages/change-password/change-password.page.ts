import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { AccountService } from 'src/app/services/account.service';
import { UiService } from 'src/app/services/ui.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  oldPassword;
  initNewPassword;
  newPassword;

  constructor(
    private nav: NavController,
    private account: AccountService,
    private ui: UiService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
  }

  cancel() {
    this.nav.back();
  }

  async changePassword() {

    if (this.initNewPassword != this.newPassword) {
      (await this.ui.alert("Password do not match.", "Change Password")).present();
      return;
    }

    if (!await this.checkPassword())
      return;

    (await this.ui.loader('Changing password')).present();

    let result: any = await this.account.changePassword(this.oldPassword, this.newPassword);

    this.ui.dismissLoader();

    console.log(result);

    if (result.responsecode != 200) {
      (await this.ui.alert('Failed to change your password.')).present();
      return;
    }

    (await this.ui.alert('Password successfully changed.')).present();
    this.router.navigate(['home'], {replaceUrl: true});

    this.ui.dismissLoader();
  }

  async checkPassword() {
    
    // let regex = /(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/g;
    let lowercase = /(?=.*[a-z])/g;
    let uppercase = /(?=.*[A-Z])/g;
    let number = /(?=.*[0-9])/g;
    let eightchar = /(?=.{8,})/g

    if (!lowercase.test(this.newPassword) || !uppercase.test(this.newPassword) || !number.test(this.newPassword) || !eightchar.test(this.newPassword)) {

      let alert_password = await this.alert.create({
        message: "Invalid password. Password must contain the following:",
        inputs: [
          {
            type: "checkbox",
            checked: lowercase.test(this.newPassword),
            label: "At least one lowercase letter.",
            cssClass: 'readonly'
          },
          {
            type: "checkbox",
            checked: uppercase.test(this.newPassword),
            label: "At least one uppercase letter.",
            cssClass: 'readonly'
          },
          {
            type: "checkbox",
            checked: number.test(this.newPassword),
            label: "At least one digit.",
            cssClass: 'readonly'
          },
          {
            type: "checkbox",
            checked: eightchar.test(this.newPassword),
            label: "At least 8 characterS.",
            cssClass: 'readonly'
          }          
        ],
        cssClass: "passwordReq"
      })

      alert_password.present();

      return false;

    } else
      return true;

  }

}
