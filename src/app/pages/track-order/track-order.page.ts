import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SearchingService } from 'src/app/services/searching.service';
import { UiService } from 'src/app/services/ui.service';
import { Router } from '@angular/router';
import { Keyboard } from '@capacitor/keyboard';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.page.html',
  styleUrls: ['./track-order.page.scss'],
})
export class TrackOrderPage implements OnInit {

  @ViewChild('trackingSlides') trackingSlide: HTMLIonSlidesElement;

  descriptor = {};
  request_requirements = [];
  type;
  trackingDetails = {
    order_no: '',
    first6: '',
    last4: '',
    firstname: '',
    lastname: '',
    time: '',
    amount: '',
    email_address: ''
  }

  constructor(
    private nav: NavController,
    private search: SearchingService,
    private ui: UiService,
    private router: Router
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    this.descriptor = state.descriptor;
    this.request_requirements = JSON.parse(this.descriptor['request_requirements']) ?? [];
    // console.log(this.request_requirements.filter(e => e.name == 'orderid').length);
    this.type = state.type;
  }

  ionViewDidEnter() {
    this.trackingSlide.lockSwipes(true);
  }

  back() {
    this.nav.back();
  }

  next() {
    this.trackingSlide.lockSwipes(false);
    this.trackingSlide.slideNext();
    this.trackingSlide.lockSwipes(true);
  }

  slideBack() {
    this.trackingSlide.lockSwipes(false);
    this.trackingSlide.slidePrev();
    this.trackingSlide.lockSwipes(true);
  }

  async trackOrder() {
    if (this.trackingDetails.order_no) {
      this.router.navigate(['track-result'], {
        state: {
          descriptor: this.descriptor,
          trackingDetails: this.trackingDetails,
          type: this.type
        }
      });

      return;
    }

    if (this.trackingDetails.email_address && !this.search.validateEmail(this.trackingDetails.email_address)) {
      (await this.ui.alert('Invalid email.')).present();
      return;
    }

    this.router.navigate(['track-result'], {
      state: {
        descriptor: this.descriptor,
        trackingDetails: this.trackingDetails,
        type: this.type
      }
    });
  }

  orderNumberSelected() {
    
    if (this.trackingDetails.order_no)
      return true;
    else return false;

  }

  otherOptionSelected() {
    
    if (this.trackingDetails.firstname || this.trackingDetails.lastname || this.trackingDetails.first6 || this.trackingDetails.last4)
      return true;
    else return false;

  }

  nextInput(e, input = null) {

    if (e.key != 'Enter')
      return;
    
    if (input)
      input.setFocus();
    else
      Keyboard.hide();
    
  }

  orderIdIsRequired() {
    return this.request_requirements.filter(e => e.name == 'orderid').length;
  }

}
