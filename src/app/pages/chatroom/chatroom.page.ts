import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { NavController, AlertController, Platform } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';
import { gsap } from 'gsap';
import { Subscription, Subscribable } from 'rxjs';
import { Keyboard } from '@capacitor/keyboard';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.page.html',
  styleUrls: ['./chatroom.page.scss'],
})
export class ChatroomPage implements OnInit {

  @ViewChild('convoBubbles') convoBubbles;
  @ViewChild('chatConvoContent') chatConvoContent;
  @ViewChild('textarea') textarea: HTMLIonTextareaElement;

  socketStatus: "connecting" | "connected" | "failed" | "hidden" = "connecting";
  private receiverSubscription: Subscription;
  private requestingSubscription: Subscription;
  private countTimer;
  private showOnTap;

  private backhardwareSub: Subscription;
  private docbackbutton = (e) => {
    e.stopPropagation();
    e.preventDefault();
  };

  clientImg;
  chatString: string;
  timer = "0:00";
  requesting = true;
  pendingMsg = [];

  constructor(
    private storage: StorageService,
    private nav: NavController,
    public chat: ChatService,
    private alert: AlertController,
    private platform: Platform
  ) { }

  ngOnInit() {

  }

  async getHistory() {
    let result: any = await this.chat.getHistory();
    let data = result.data;
    let convoBubbles = this.convoBubbles.nativeElement;

    let cbInner = convoBubbles.innerHTML;
    convoBubbles.innerHTML = '';

    if (data) {

      for (let i of data) {
        console.log(i);
        let history = i.chat_history
        try {
          convoBubbles.innerHTML += atob(history);
        } catch { }
      }

      /** 
       * 
       *  FORMAT AGENT BUBBLES
       * 
       */
      let agent_msgs = document.querySelectorAll('.agent_msg');

      for (let msg in agent_msgs) {

        if (typeof agent_msgs[msg] != 'object')
          continue;
      
        try {
          let inner = agent_msgs[msg].innerHTML;
          let agent_name = agent_msgs[msg].querySelector('.agentname b').textContent.replace(/Agent/g, '').trim();

          agent_msgs[msg].innerHTML = `<div class="img">
                                      <span>${agent_name.charAt(0)}</span>
                                    </div>` + inner;
        } catch { }
      }

      /**
       * 
       * FORMAT CLIENT BUBBLES
       * 
       */
      let client_msgs = document.querySelectorAll('.clients_msg:not(.alert-danger)');

      for (let msg in client_msgs) {
        if (typeof client_msgs[msg] != 'object')
          continue;
      
        console.log(client_msgs[msg]);
        let icon = client_msgs[msg].querySelector('.client_icon');
        icon.setAttribute('src', this.clientImg);
      }
    }

    console.log('passed');

    convoBubbles.innerHTML += cbInner;

    this.scrollToBottom();

    this.establishConnection();

  }

  async ionViewWillEnter() {
    let client: any = await this.storage.get('user_info');
    if (client != "guess") {
      this.clientImg = client.image_url;
    }

    this.backhardwareSub = this.platform.backButton.subscribeWithPriority(99, () => {
      document.addEventListener('backbutton', this.docbackbutton);
      this.back();
    })
  }

  ionViewDidEnter() {
    this.getHistory();
  }

  establishConnection() {
    let chat = this.chat.EstablishConnection();
    chat.then(e => {

      this.socketStatus = "connected";
      setTimeout(() => {
        this.socketStatus = 'hidden';
      }, 3000);
      this.requestAgent();
      this.initReceiver();

    }).catch(e => {
      
      this.socketStatus = "failed";

    })

    this.setShowInfoOnTap();
  }

  async requestAgent() {
    await this.chat.requestAgent();

    let count = 0;

    this.countTimer = setInterval(() => {
      count++;
      let min = Math.floor(count / 60).toString();
      let sec = (count % 60).toString();
      this.timer = `${min.length > 1 ? min : '0' + min}:${sec.length > 1 ? sec : '0' + sec}`;
    }, 1000);

    this.requestingSubscription = this.chat.requesting.subscribe(e => {
      if (!e) {
        this.requesting = false;
        clearInterval(this.countTimer);
      }
    })
  }

  initReceiver() {
    this.receiverSubscription = this.chat.onReceive.subscribe(e => {

      console.log(e);
      
      if (!e)
        return;
      
      if (e.request_code != this.chat.client.id)
        return;
      
      if (e.request_from != 'ican')
        return;
      
      // this.chatConvoContent.scrollToBottom(200);      

      if (e.request_status && e.request_status == "end") {

        this.chat.chatEnded = true;

        let convoBubbles = this.convoBubbles.nativeElement.querySelector('.new-convo-bubbles');
        convoBubbles.innerHTML += `
          <div class="clients_msg mt-1 mb-1 text-center alert-danger p-1">
            <b>${this.chat.agent}</b> ended the chat<br>
            <span class="date_time"><i>${e.request_date}</i></span>
          </div>
        `;

        this.scrollToBottom();
        return;
        
      }
      
      if (e.user_id) {

        if (e.type == "Typing_true" || e.type == "Typing_false")
          return;
        
        let convoBubbles = this.convoBubbles.nativeElement.querySelector('.new-convo-bubbles');

        convoBubbles.innerHTML += `
          <div class="agent_msg p-1 42">
            <div class="img">
              <span>${e.user_name.charAt(0)}</span>
            </div>
            <img alt="Logo" class="float-right ml-1 agent_icon hide" src="https://chat.ican4consumers.com/public/img/client.png">
            <div class="float-right msg_content fromagent">
              <span class="float-right agentname">
                <b>Agent ${e.user_name}</b>
              </span>
              <br>
              <span class="right mt-0 agentmsg p-2">${e.msg}</span>
              <br>
              <span class="float-right date_time agenttime"><i>${e.date_chat}</i></span>
              <br>
              <br>
            </div>
          </div>
        `;

      } else {

        if (e.type != 'send_message')
          return;

        let msg = e.msg;
        let msg_index = this.pendingMsg.indexOf(msg);
        this.pendingMsg.splice(msg_index, 1);

        let convoBubbles = this.convoBubbles.nativeElement.querySelector('.new-convo-bubbles');
        convoBubbles.innerHTML += `
          <div class="clients_msg p-1 ">
            <img class="float-left mr-1 mt-3 client_icon" src="${this.clientImg}" alt="Logo">
            <div class="float-left msg_content fromclient">
              <span class="float-left aclientname">
                  <b>${this.chat.client.name.split('-')[0].trim()}</b>
              </span>
              <br>
              <span class="left mt-0 clientmsg p-2">${msg}</span>
              <br>
              <span class="float-left date_time clienttime"><i>${e.date_chat}</i></span>
            </div>
          </div>
        `;

      }

      
      this.scrollToBottom();

    })
  }

  async sendChat() {
    // await Keyboard.hide();
    this.pendingMsg.push(this.chatString);
    // this.chatConvoContent.scrollToBottom(200);
    this.scrollToBottom();
    this.chat.sendMessage(this.chatString);
    this.chatString = '';
    this.textarea.setFocus();
  }

  setShowInfoOnTap() {
    this.showOnTap = function (e) {
      let target: any = e.target;

      if (target.className.includes('agentmsg') || target.className.includes("clientmsg")) {
        
        let parent = target.closest('.msg_content');
        let info = parent.querySelector('.date_time');

        console.log(getComputedStyle(info).height);
        if (getComputedStyle(info).height == "0px") {

          gsap.to('.agent_msg .date_time, .clients_msg .date_time', {
            height: 0
          })

          gsap.to(info, {
            height: 20
          })

        }
        else
        gsap.to(info, {
          height: 0
        })

      }
    }
    document.addEventListener('click', this.showOnTap);
  }

  triggerTyping() {
    
    if (this.chatString) {
      this.chat.triggerTyping(true);
    } else {
      this.chat.triggerTyping(false);
    }

  }

  async back() {

    let endAlert = await this.alert.create({
      message: "End chat?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        },
        {
          text: "OK",
          role: "end"
        }
      ]
    });

    if (!this.chat.chatEnded) {
      endAlert.present();

      let role = (await endAlert.onDidDismiss()).role

      if (role == "cancel")
        return;
    }
    
    let new_chats_div = document.querySelector('.new-convo-bubbles');
    let history = new_chats_div.innerHTML;
    
    if (history && !this.chat.chatEnded) {
      let convoBubbles = this.convoBubbles.nativeElement.querySelector('.new-convo-bubbles');
      convoBubbles.innerHTML += `
        <div class="clients_msg mt-1 mb-1 text-center alert-danger p-1">
          <b>${this.chat.client.name.split('-')[0].trim()}</b> ended the chat<br>
          <span class="date_time"><i>${this.chat.datetime()}</i></span>
        </div>
      `;
      // this.chatConvoContent.scrollToBottom(400);
      this.scrollToBottom();

    }
    
    try {
      clearInterval(this.countTimer);
      document.removeEventListener('click', this.showOnTap);
      this.receiverSubscription.unsubscribe();
      this.requestingSubscription.unsubscribe();
    } catch { }
    
    try {
      await this.chat.endChat();
    } catch { }

    this.backhardwareSub.unsubscribe();
    document.removeEventListener('backbutton', this.docbackbutton);
    this.nav.back();
  }

  scrollToBottom() {
    
    let elem = this.chatConvoContent.nativeElement;
    elem.scroll({
      top: elem.scrollHeight,
      behavior: 'smooth'
    });

  }

}
