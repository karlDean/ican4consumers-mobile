import { Component, OnInit } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { AlertController, NavController } from '@ionic/angular';
import { AccountService } from 'src/app/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  signUpData = {
    username:"",
    first_name:"",
    last_name:"",
    main_email_address:"",
    password:"",
    notification_promo: "",
    notification_newmerch: ""
  };
  password_init = "";
  googleMode = false;

  constructor(
    private ui: UiService,
    private alert: AlertController,
    private account: AccountService,
    private router: Router,
    private nav: NavController
  ) { }

  ngOnInit() {
  }

  async signUp() {

    (await this.ui.loader()).present();

    if (!await this.checkUsername())
      return;
    
    if (!await this.checkPassword())
      return;

    if (!await this.signUpOpts())
      return;
    
    (await this.ui.loader('Signing you up...')).present();
    
    const signup: any = await this.account.signUp(this.signUpData);

    this.ui.dismissLoader();

    if (signup.responsecode != 200)
      (await this.ui.alert(signup.responsemsg, 'Sign up')).present();
    else {
      (await this.alert.create({
        header: "Sign up",
        message: 'Account created successfully.',
        buttons: [
          {
            text: "Login now",
            handler: () => {
              this.router.navigate(['login']);
            }
          }
        ]
      })).present()
    }
  }

  async signUpOpts() {

    if (!await this.confirmPassword())
      return;
    
    this.ui.dismissLoader();
    
    const opts = await this.alert.create({
      message: 'Notifications',
      backdropDismiss: false,
      inputs: [
        {
          type: 'checkbox',
          label: 'I would like to receive promotional offering from iCan Member Merchants.',
          name: 'newmerch',
          value: 'newmerch'
        },
        {
          type: 'checkbox',
          label: 'Please let me know when new merchants become a member of the iCan Network',
          name: 'promo',
          value: 'promo'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Proceed',
          role: 'signup'
        }
      ]
    });

    opts.present();

    let result = await opts.onDidDismiss();
    if (result.role == 'cancel')
      return new Promise((e, reject) => reject(0));
    
    let values = result.data.values;

    this.signUpData.notification_newmerch = values.includes('newmerch') ? 'y' : 'n';
    this.signUpData.notification_promo = values.includes('promo') ? 'y' : 'n';
    return new Promise(resolve => resolve(1));
  }

  allFilled() {
    
    for (let data in this.signUpData) {
      if (['notification_promo', 'notification_newmerch'].includes(data))
        continue;
      if (!this.signUpData[data]) {
        return true;
      }
    }
    
    return false;

  }

  async confirmPassword() {

    if (this.password_init !== this.signUpData.password) {

      this.ui.dismissLoader();

      (await this.ui.alert("Password don't match")).present();

      return false;
    }

    return true;
  }

  async checkCreds() {

    (await this.ui.loader()).present();

    let google: any = await this.account.getGoogleCredentials().catch(async e => {
      (await this.ui.toast(e)).present();
      this.ui.dismissLoader();
    });

    if (!google)
      return;

    this.ui.dismissLoader();

    this.signUpData.first_name = google.given_name;
    this.signUpData.last_name = google.family_name;
    this.signUpData.main_email_address = google.email;

    this.googleMode = true;
  }

  async signUpGoogle() {
    
    if (!await this.confirmPassword())
      return;
      
    this.signUp();
  }

  async checkUsername() {
    let result: any = await this.account.checkUsername(this.signUpData.username);

    if (result.responsecode == -1) {
      this.ui.dismissLoader();
      (await this.ui.alert('Username already taken.')).present();
      return false;
    }

    return true;
  }

  async checkPassword() {
    
    // let regex = /(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/g;
    let lowercase = /(?=.*[a-z])/g;
    let uppercase = /(?=.*[A-Z])/g;
    let number = /(?=.*[0-9])/g;
    let eightchar = /(?=.{8,})/g

    if (!lowercase.test(this.signUpData.password) || !uppercase.test(this.signUpData.password) || !number.test(this.signUpData.password) || !eightchar.test(this.signUpData.password)) {

      this.ui.dismissLoader();

      let alert_password = await this.alert.create({
        message: "Invalid password. Password must contain the following:",
        inputs: [
          {
            type: "checkbox",
            checked: lowercase.test(this.signUpData.password),
            label: "At least one lowercase letter.",
            cssClass: 'readonly'
          },
          {
            type: "checkbox",
            checked: uppercase.test(this.signUpData.password),
            label: "At least one uppercase letter.",
            cssClass: 'readonly'
          },
          {
            type: "checkbox",
            checked: number.test(this.signUpData.password),
            label: "At least one digit.",
            cssClass: 'readonly'
          },
          {
            type: "checkbox",
            checked: eightchar.test(this.signUpData.password),
            label: "At least 8 characterS.",
            cssClass: 'readonly'
          }          
        ],
        cssClass: "passwordReq"
      })

      alert_password.present();

      return false;

    } else
      return true;

  }

  back() {
    this.nav.back();
  }

}
