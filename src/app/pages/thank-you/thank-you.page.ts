import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.page.html',
  styleUrls: ['./thank-you.page.scss'],
})
export class ThankYouPage implements OnInit {

  constructor(
    private router: Router,
    private ui: UiService
  ) { }

  ngOnInit() {
  }

  returnHome() {
    this.router.navigate(['home']);
  }

  call() {
    this.ui.dial("8556603124");
  }

  chat() {
    this.router.navigate(['chatroom']);
  }

  email() {
    this.router.navigate(['contact-us'], { state: { autoOpen: true, redirected: true } });
  }

}
