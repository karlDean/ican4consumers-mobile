import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { EmailerComponent } from 'src/app/components/emailer/emailer.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})
export class ContactUsPage implements OnInit {

  redirected = false;
  autoOpen = false;

  constructor(
    private modal: ModalController,
    private router: Router,
    private nav: NavController
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    if (state && state.redirected)
      this.redirected = true;
    if (state && state.autoOpen)
      this.autoOpen = true;
  }

  ionViewDidEnter() {
    if (this.autoOpen)
      this.composeEmail();
  }

  async composeEmail() {
    let modal = await this.modal.create({
      component: EmailerComponent,
      cssClass: 'emailer'
    });
    modal.present();
  }

  back() {
    this.nav.back();
  }

  ionViewDidLeave() {

  }

  ngOnDestroy() {
    
  }

}
