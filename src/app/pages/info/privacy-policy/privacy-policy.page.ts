import { Component, OnInit, ViewChild } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss'],
})
export class PrivacyPolicyPage implements OnInit {

  @ViewChild('privacyPolicyContent') privacyPolicyContent;
  redirected = false;

  constructor(
    private nav: NavController,
    private router: Router
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    if (state && state.redirected)
      this.redirected = true;
  }

  async openSource() {
    await Browser.open({
      url: 'https://ican4consumers.com/privacy-policy.php',
      presentationStyle: "popover",
      toolbarColor: '#4279FC'
    })
  }

  back() {
    this.nav.back();
  }

}
