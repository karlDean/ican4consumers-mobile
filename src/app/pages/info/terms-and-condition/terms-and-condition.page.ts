import { Component, OnInit, ViewChild } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-terms-and-condition',
  templateUrl: './terms-and-condition.page.html',
  styleUrls: ['./terms-and-condition.page.scss'],
})
export class TermsAndConditionPage implements OnInit {

  @ViewChild('termsAndConditionContent') termsAndConditionContent;
  redirected = false;

  constructor(
    private router: Router,
    private nav: NavController
  ) { }

  ngOnInit() {
    let state = this.router.getCurrentNavigation().extras.state;
    if (state && state.redirected)
      this.redirected = true;
  }

  async openSource() {
    await Browser.open({
      url: 'https://ican4consumers.com/terms-and-conditions.php',
      presentationStyle: "popover",
      toolbarColor: '#4279FC'
    });
  }

  back() {
    this.nav.back();
  }

}
