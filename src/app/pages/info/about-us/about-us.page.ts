import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {

  @ViewChild('aboutUsContent') aboutUsContent;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  
  refund() {
    this.router.navigate(['refund/request']);
  }

}
