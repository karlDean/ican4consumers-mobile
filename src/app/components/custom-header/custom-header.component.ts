import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-custom-header',
  templateUrl: './custom-header.component.html',
  styleUrls: ['./custom-header.component.scss'],
})
export class CustomHeaderComponent implements OnInit {

  constructor(
    private menu: MenuController
  ) { }

  ngOnInit() { }
  
  openMenu() {
    this.menu.open();
  }

  closeMenu() {
    console.log('asd');
  }

}
