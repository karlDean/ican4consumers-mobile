import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';
import { environment } from 'src/environments/environment';
import { ModalController } from '@ionic/angular';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-emailer',
  templateUrl: './emailer.component.html',
  styleUrls: ['./emailer.component.scss'],
})
export class EmailerComponent implements OnInit {

  emailDetails = {
    first_name: '',
    last_name: '',
    email: '',
    message: '',
    phone: ''
  }

  constructor(
    private request: RequestService,
    private modal: ModalController,
    private ui: UiService
  ) { }

  ngOnInit() { }
  
  async sendEmail() {
    
    let params = {
      request_type: 'contactUs',
      api_key: environment.api_key,
      ...this.emailDetails
    };
    
    (await this.ui.loader('Sending')).present();

    let result: any = await this.request.get(environment.refund_api, params);

    this.ui.dismissLoader();

    if (result.responsecode == 200) {
      (await this.ui.alert('Email sent.')).present();
      await this.ui.alertDidDismiss();
      this.modal.dismiss();
    } else {
      (await this.ui.alert('Failed to send email. Please try again after some time.')).present();
    }

  }

  cancel() {
    this.modal.dismiss();
  }

}
