import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-unreceived-svg',
  templateUrl: './unreceived-svg.component.html',
  styleUrls: ['./unreceived-svg.component.scss'],
})
export class UnreceivedSvgComponent implements OnInit {
  
  @Input() entrance: string = 'true';

  constructor() { }

  ngOnInit() {}

}
