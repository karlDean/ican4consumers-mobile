import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RefundingService } from 'src/app/services/refunding.service';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-refund-history',
  templateUrl: './refund-history.component.html',
  styleUrls: ['./refund-history.component.scss'],
})
export class RefundHistoryComponent implements OnInit {

  @Input() refund;
  cardNetwork: string;

  constructor(
    private modal: ModalController,
    private refundService: RefundingService
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {

    this.refund['reason'] = this.refund['reason'].replace(/\\/g, '');
    
    let card_bin = this.refund.card_number.substring(0, 6);
    this.refundService.getCardNetwork(card_bin)
      .then((e: any) => {
        this.cardNetwork = e.scheme;
      })
      .catch(e => console.log(e));
  }
  
  dismissModal() {
    this.modal.dismiss();
  }

  parseDate(time) {
    let date: any;

    try {
      date = new Date(time);
      let dateString = date.toLocaleDateString('en-US', { month: 'short', day: 'numeric', year: 'numeric' });
      return dateString;
    } catch {
      date = time.substring(0, 8);
      date = `${date.substring(0, 4)}-${date.substring(4, 6)}-${date.substring(6)}`;
      date = new Date(date);
      let dateString = date.toLocaleDateString('en-US', { month: 'short', day: 'numeric', year: 'numeric' });
      return dateString;
    }
  }

  formatCard(card: string) {

    card = card.replace(/X/gi, '•');
    let card_arr = card.match(/.{1,4}/g);
    card = card_arr.join(' ');
    return card;
    
  }

}
