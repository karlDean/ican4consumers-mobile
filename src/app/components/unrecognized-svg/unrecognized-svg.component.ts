import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-unrecognized-svg',
  templateUrl: './unrecognized-svg.component.html',
  styleUrls: ['./unrecognized-svg.component.scss'],
})
export class UnrecognizedSvgComponent implements OnInit {

  @Input() entrance: string = 'true';

  constructor() { }

  ngOnInit() {}

}
