import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { SearchingService } from 'src/app/services/searching.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-descriptor-search',
  templateUrl: './descriptor-search.component.html',
  styleUrls: ['./descriptor-search.component.scss'],
})
export class DescriptorSearchComponent implements OnInit {

  public recentSearches = [];
  public descriptor_key: string;
  public searchResults = [];
  public searching = false;

  constructor(
    private modal: ModalController,
    private storage: StorageService,
    private search: SearchingService,
    private alert: AlertController,
    private router: Router
  ) { }

  ngOnInit() { }

  async ionViewDidEnter() {
    let recentDescriptors: any = await this.storage.get('recentDescriptors');
    this.recentSearches = recentDescriptors ? [...recentDescriptors] : [];
  }
  
  closeModal() {
    this.modal.dismiss(null, 'cancel');
  }

  async searchDescriptor() {

    this.searchResults = [];

    if (!this.descriptor_key || this.descriptor_key.length < 5)
      return;
    
    this.searching = true;
    
    let result: any = await this.search.descritpor(this.descriptor_key);
    this.searchResults = result;

    this.searching = false;
    
  }

  selectDescriptor(descriptor) {

    if (!this.recentSearches.includes(descriptor.descriptor_text)) {
      if (this.recentSearches.length < 5) {
        this.recentSearches.unshift(descriptor.descriptor_text);
      } else {
        this.recentSearches.unshift(descriptor.descriptor_text);
        this.recentSearches.pop();
      }

      this.storage.set({ recentDescriptors: this.recentSearches });
    }

    this.modal.dismiss(descriptor, 'select');

  }

  autofill(descriptor) {
    this.descriptor_key = descriptor;
  }

  async clearSearches() {
    
    (await this.alert.create({
      message: "Clear recent searches?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Clear',
          handler: () => {
            this.storage.remove('recentDescriptors');
            this.recentSearches = [];
          }
        }
      ]
    })).present();

  }

  nmmRefund() {
    this.modal.dismiss(null, 'nmm');
    // this.modal.
    this.router.navigate(['refund/nmm'], {state: {merch_info: this.descriptor_key}});
  }

}
