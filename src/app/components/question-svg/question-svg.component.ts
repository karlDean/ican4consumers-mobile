import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-svg',
  templateUrl: './question-svg.component.html',
  styleUrls: ['./question-svg.component.scss'],
})
export class QuestionSVGComponent implements OnInit {

  @Input() type: string;
  @Input() entrance: string = 'true';

  constructor() { }

  ngOnInit() {}

}
