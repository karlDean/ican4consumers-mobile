import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-txn-picker',
  templateUrl: './txn-picker.component.html',
  styleUrls: ['./txn-picker.component.scss'],
})
export class TxnPickerComponent implements OnInit {

  @Input() txns = [];
  @Input() api_type = false;

  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() { }
  
  ionViewDidEnter() {
    

  }

  parseDate(time) {
    let date: any;

    try {
      date = new Date(time);
      let dateString = date.toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' });
      return dateString;
    } catch {
      date = time.substring(0, 8);
      date = `${date.substring(0, 4)}-${date.substring(4, 6)}-${date.substring(6)}`;
      date = new Date(date);
      let dateString = date.toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' });
      return dateString;
    }

  }

  pickTxn(id) {
    
    let selected = this.txns.find(e => e.transactionid == id);
    this.modal.dismiss(selected);

  }

}
