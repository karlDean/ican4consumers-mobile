import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-custom-footer',
  templateUrl: './custom-footer.component.html',
  styleUrls: ['./custom-footer.component.scss'],
})
export class CustomFooterComponent implements OnInit {
  
  @Input() homeSlide: any;
  @Input() content: any;
  activated_tab = 'home';

  constructor(
    private platform: Platform,
    private router: Router
  ) { }

  ngOnInit() { }
  
  viewBox() {
    let width = this.platform.width();

    return "0 0 " + width + " 60";
  }

  path() {

    let mid = this.platform.width();
    
    return `M 0 0 L ${(mid/2) - 80} 0 q 30 0 40 22.5 q 10 20 40 22.5 q 30 -2.5 40 -22.5 q 10 -22.5 40 -22.5 l ${(mid/2) - 80} 0 l 0 60 l -${mid} 0 Z`
  }

  account() {
    this.homeSlide.lockSwipes(false);
    this.homeSlide.slideTo(1);
    this.content.scrollToTop(400);
    this.homeSlide.lockSwipes(true);
    this.activated_tab = 'account';
  }

  async home() {
    this.homeSlide.lockSwipes(false);
    this.homeSlide.slideTo(0);
    this.content.scrollToTop(400);
    this.homeSlide.lockSwipes(true);
    this.activated_tab = 'home';
  }

  openChat() {
    this.router.navigate(['chatroom']);
  }

}
