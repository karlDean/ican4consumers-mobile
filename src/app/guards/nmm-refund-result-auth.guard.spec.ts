import { TestBed } from '@angular/core/testing';

import { NmmRefundResultAuthGuard } from './nmm-refund-result-auth.guard';

describe('NmmRefundResultAuthGuard', () => {
  let guard: NmmRefundResultAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NmmRefundResultAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
