import { TestBed } from '@angular/core/testing';

import { ProductQuestionAuthGuard } from './product-question-auth.guard';

describe('ProductQuestionAuthGuard', () => {
  let guard: ProductQuestionAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ProductQuestionAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
