import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrackResultAuthGuardGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      let routeState = this.router.getCurrentNavigation().extras.state;

      if (!routeState || !routeState.descriptor || !routeState.type || !routeState.trackingDetails)
        return this.router.parseUrl('track-order');
      
      return true;
  }
  
}
