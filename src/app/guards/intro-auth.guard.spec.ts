import { TestBed } from '@angular/core/testing';

import { IntroAuthGuard } from './intro-auth.guard';

describe('IntroAuthGuard', () => {
  let guard: IntroAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IntroAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
