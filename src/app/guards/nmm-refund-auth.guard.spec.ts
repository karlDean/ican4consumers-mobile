import { TestBed } from '@angular/core/testing';

import { NmmRefundAuthGuard } from './nmm-refund-auth.guard';

describe('NmmRefundAuthGuard', () => {
  let guard: NmmRefundAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NmmRefundAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
