import { TestBed } from '@angular/core/testing';

import { TrackResultAuthGuardGuard } from './track-result-auth-guard.guard';

describe('TrackResultAuthGuardGuard', () => {
  let guard: TrackResultAuthGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TrackResultAuthGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
