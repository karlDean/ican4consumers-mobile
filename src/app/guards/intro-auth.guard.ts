import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, from } from 'rxjs';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class IntroAuthGuard implements CanActivate {

  constructor(
    private storage: StorageService,
    private router: Router
  ){}
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return from(this.checkFirstVisit());
  }
  
  async checkFirstVisit(): Promise<boolean | UrlTree> {
    
    return new Promise(resolve => {
      this.storage.get('intro').then(e => {
        if (e)
          resolve(this.router.parseUrl('home'));
        else
          resolve(true);          
      })
    })
  }
}
