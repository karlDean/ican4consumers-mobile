import { TestBed } from '@angular/core/testing';

import { TrackOrderAuthGuard } from './track-order-auth.guard';

describe('TrackOrderAuthGuard', () => {
  let guard: TrackOrderAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TrackOrderAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
