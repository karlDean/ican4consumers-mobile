import { TestBed } from '@angular/core/testing';

import { CancellationResultAuthGuard } from './cancellation-result-auth.guard';

describe('CancellationResultAuthGuard', () => {
  let guard: CancellationResultAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CancellationResultAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
