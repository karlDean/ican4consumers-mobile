import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, from } from 'rxjs';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserAuthGuard implements CanActivate {

  constructor(
    private storage: StorageService,
    private router: Router
  ){ }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    return from(this.validateUser());
  }

  async validateUser(): Promise<boolean | UrlTree> {

    return new Promise(resolve => {
      this.storage.get('user_info').then((e: any) => {
        if (e)
          resolve(true);
        else
          resolve(this.router.parseUrl('login'));
      })
    })

  }
  
}
