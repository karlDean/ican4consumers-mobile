import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UserAuthGuard } from './guards/user-auth.guard';
import { IntroAuthGuard } from './guards/intro-auth.guard';
import { ProductQuestionAuthGuard } from './guards/product-question-auth.guard';
import { TrackOrderAuthGuard } from './guards/track-order-auth.guard';
import { CancellationResultAuthGuard } from './guards/cancellation-result-auth.guard';
import { TrackResultAuthGuardGuard } from './guards/track-result-auth-guard.guard';
import { NmmRefundAuthGuard } from './guards/nmm-refund-auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    canActivate: [UserAuthGuard]
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/intro/intro.module').then(m => m.IntroPageModule),
    canActivate: [IntroAuthGuard]
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./pages/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'product-question',
    loadChildren: () => import('./pages/product-question/product-question.module').then(m => m.ProductQuestionPageModule),
    canActivate: [ProductQuestionAuthGuard]
  },
  {
    path: 'track-order',
    loadChildren: () => import('./pages/track-order/track-order.module').then(m => m.TrackOrderPageModule),
    canActivate: [TrackOrderAuthGuard]
  },
  {
    path: 'track-result',
    loadChildren: () => import('./pages/track-result/track-result.module').then( m => m.TrackResultPageModule),
    canActivate: [TrackResultAuthGuardGuard]
  },
  {
    path: 'thank-you',
    loadChildren: () => import('./pages/thank-you/thank-you.module').then( m => m.ThankYouPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./pages/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'cancellation-result',
    loadChildren: () => import('./pages/cancellation-result/cancellation-result.module').then(m => m.CancellationResultPageModule),
    canActivate: [CancellationResultAuthGuard]
  },
  {
    path: 'refund',
    loadChildren: () => import('./pages/refund/index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'refund/request',
    loadChildren: () => import('./pages/refund/request/request.module').then( m => m.RequestPageModule)
  },
  {
    path: 'refund/result',
    loadChildren: () => import('./pages/refund/result/result.module').then( m => m.ResultPageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./pages/info/about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./pages/info/contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./pages/info/privacy-policy/privacy-policy.module').then( m => m.PrivacyPolicyPageModule)
  },
  {
    path: 'terms-and-condition',
    loadChildren: () => import('./pages/info/terms-and-condition/terms-and-condition.module').then( m => m.TermsAndConditionPageModule)
  },
  {
    path: 'refund/support',
    loadChildren: () => import('./pages/refund/support/support.module').then( m => m.SupportPageModule)
  },
  {
    path: 'chatroom',
    loadChildren: () => import('./pages/chatroom/chatroom.module').then( m => m.ChatroomPageModule)
  },
  {
    path: 'refund/nmm',
    loadChildren: () => import('./pages/refund/nmm/nmm.module').then( m => m.NmmPageModule),
    canActivate: [NmmRefundAuthGuard]
  },
  {
    path: 'refund/nmm-result',
    loadChildren: () => import('./pages/refund/nmm-result/nmm-result.module').then(m => m.NmmResultPageModule)
  },
  {
    path: 'refund/history',
    loadChildren: () => import('./pages/refund/history/history.module').then( m => m.HistoryPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
