import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { environment } from './../../environments/environment';
import { GooglePlusService } from './google-plus.service';
import { StorageService } from './storage.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public accountChanged = new BehaviorSubject(null);

  constructor(
    private request: RequestService,
    private google: GooglePlusService,
    private storage: StorageService
  ) { }

  async login(data: { username: string, password: string }) {
    
    const result = await this.request.get(environment.refund_api, {
      ...data,
      request_type: 'signIn',
      api_key: environment.api_key
    });

    return new Promise(resolve => {
      resolve(result);
    })
  }

  async googleLogin() {
    const google: any = await this.google.login();
    return new Promise(async (resolve, reject) => {

      if (!google) {
        reject('Failed to login');
        return;
      }

      let profile;

      try {
        profile = await this.request.get(environment.refund_api, {
          request_type: 'loginWithGoogle',
          api_key: environment.api_key,
          email: google.email,
          google_id: google.sub ? google.sub : google.id
        });
      } catch {
        profile = null;
      }

      resolve(profile);
      
    });
  }

  async signUp(data: object) {
    const result = await this.request.get(environment.refund_api, {
      request_type: 'signUp',
      api_key: environment.api_key,
      ...data
    });
    return new Promise(resolve => resolve(result));
  }

  async getGoogleCredentials() {
    
    return new Promise(async (resolve, reject) => {

      const google: any = await this.google.login().catch(e => reject('Sign up cancelled.'));

      if (!google) {
        reject('Failed to login');
        return;
      }

      let profile: any = await this.request.get(environment.refund_api, {
        request_type: 'loginWithGoogle',
        api_key: environment.api_key,
        email: google.email,
        google_id: google.sub ? google.sub : google.id
      }).catch(e => {
        resolve(google);
      });

      if (!profile)
        return;

      if (profile.responsecode == 200) {
        reject('Email is already registered.');
        return;
      }

      resolve(google);
    })
  }

  async checkUsername(username: string) {
    let result = await this.request.get(environment.refund_api, {
      request_type: 'checkUsername',
      api_key: environment.api_key,
      username: username
    });

    return new Promise(resolve => resolve(result));
  }

  async getOtherEmails() {

    let mobile_token = (await this.storage.get('user_info'))['mobile_token'];
    
    let result: any = await this.request.get(environment.refund_api, {
      request_type: 'getProfile',
      api_key: environment.api_key,
      token: mobile_token
    });

    return new Promise(resolve => resolve(result.responsedetails.email_other));
  }

  async modifyOtherEmails(otherEmails: Array<string>) {

    otherEmails = otherEmails.map(e => `"${e}"`);
    
    let mobile_token = (await this.storage.get('user_info'))['mobile_token'];

    let result: any = await this.request.get(environment.refund_api, {
      request_type: 'modifyOtherEmails',
      api_key: environment.api_key,
      token: mobile_token,
      other_email_address: otherEmails.join(',')
    });

    return result;

  }

  async logout() {
    await this.storage.remove('user_info', 'other_emails', 'recentDescriptors');
    await this.google.logout();
    return new Promise(resolve => resolve(true));
  }

  async changePassword(oldPassword: string, newPassword: string) {
    
    let profile: any = await this.storage.get('user_info');
    let mobile_token = profile.mobile_token;

    console.log({
      request_type: 'modifyPassword',
      api_key: environment.api_key,
      token: mobile_token,
      old_password: oldPassword,
      new_password: newPassword
    });

    let result = await this.request.get(environment.refund_api, {
      request_type: 'modifyPassword',
      api_key: environment.api_key,
      token: mobile_token,
      old_password: oldPassword,
      new_password: newPassword
    });

    return new Promise(resolve=>resolve(result));

  }
}
