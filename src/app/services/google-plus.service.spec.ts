import { TestBed } from '@angular/core/testing';

import { GooglePlusService } from './google-plus.service';

describe('GooglePlusService', () => {
  let service: GooglePlusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GooglePlusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
