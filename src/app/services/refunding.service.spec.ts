import { TestBed } from '@angular/core/testing';

import { RefundingService } from './refunding.service';

describe('RefundingService', () => {
  let service: RefundingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RefundingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
