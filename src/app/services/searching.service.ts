import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { environment } from 'src/environments/environment';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SearchingService {

  constructor(
    private request: RequestService,
    private alert: AlertController,
    private router: Router
  ) { }

  async descritpor(key: string) {
    
    let result = await this.request.get(environment.refund_api, {
      request_type: 'searchDescriptor',
      api_key: environment.api_key,
      descriptor: key
    });

    return new Promise(resolve => resolve(result));

  }

  async transaction(trackingDetails, descriptor, type) {
    
    if (await this.checkSpecialProcessTypes(trackingDetails, descriptor, type))
      return new Promise(resolve => resolve(false));
    
    if (trackingDetails.order_no) {
      
      let result = await this.request.get(environment.refund_api, {
        request_type: 'search',
        api_key: environment.api_key,
        descriptor: descriptor.descriptor_text,
        order_no: trackingDetails.order_no
      });

      return new Promise(resolve=>resolve(result));

    } else {

      delete trackingDetails.order_no;
      trackingDetails.time = trackingDetails.time.split('T')[0];

      Object.keys(trackingDetails).forEach(e => trackingDetails[e] == "" && delete trackingDetails[e]);

      console.log(trackingDetails);

      let result = await this.request.get(environment.refund_api, {
        request_type: 'search',
        api_key: environment.api_key,
        descriptor: descriptor.descriptor_text,
        ...trackingDetails
      });

      return new Promise(resolve => resolve(result));

    }

  }

  async membership(trackingDetails, descriptor, type) {
    
    if (await this.checkSpecialProcessTypes(trackingDetails, descriptor, type))
      return new Promise(resolve => resolve(false));
    
    trackingDetails.time = trackingDetails.time.split('T')[0];

    Object.keys(trackingDetails).forEach(e => trackingDetails[e] == "" && delete trackingDetails[e]);

    let result = await this.request.get(environment.refund_api, {
      request_type: 'search',
      api_key: environment.api_key,
      descriptor: descriptor.descriptor_text,
      ...trackingDetails
    });

    return new Promise(resolve => resolve(result));
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async checkSpecialProcessTypes(trackingDetails, descriptor, type) {

    const PROCESS_TYPES = ['EMAIL-BASED', 'FORWARDING', 'BATCH-FILE', 'ORR2', 'UPLOAD'];

    if (!PROCESS_TYPES.includes(descriptor.process_type.toUpperCase()))
      return new Promise(resolve => resolve(false));

    let track_unable = await this.alert.create({
      header: "Unable to track order.",
      message: "We are unable to track your order, but we can contact the merchant on your behalf if you would like to get a refund based on the information you've submitted. Would you like to request a refund?.",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.router.navigate(['thank-you']);
          }
        }, {
          text: "Request a refund.",
          handler: () => {
            this.router.navigate(['refund/request'], {state: {
              trackingDetails: trackingDetails,
              descriptor: descriptor,
              type: type
            }});
          }
        }
      ]
    });
    track_unable.present();
    await track_unable.onDidDismiss();
    return new Promise(resolve=> resolve(true));
  }
}
