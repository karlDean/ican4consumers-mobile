import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { Device } from '@capacitor/device';
import firebase from 'firebase/compat/app';

@Injectable({
  providedIn: 'root'
})
export class GooglePlusService {

  constructor(
    private afAuth: AngularFireAuth,
    private gplus: GooglePlus
  ) { }

  private async webGoogle() {
    
    const provider = new firebase.auth.GoogleAuthProvider();
    const googleAuth = await this.afAuth.signInWithPopup(provider);

    return new Promise(resolve => resolve(googleAuth.additionalUserInfo.profile));

  }

  private async nativeGoogle() {
    
    const gplusUser = await this.gplus.login({
      webClientId: '511599695525-ta1imjf08nee0oj3jj42d6hik3iqkevj.apps.googleusercontent.com',
      offline: 'true',
      scopes: 'profile email'
    });

    const provider = firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken);
    const googleAuth = await this.afAuth.signInAndRetrieveDataWithCredential(provider);
    
    return new Promise(resolve => resolve(googleAuth.additionalUserInfo.profile));

  }

  async login() {

    await this.logout();

    const platform = (await Device.getInfo()).platform;
    let account;

    if (platform == 'web') {
      account = await this.webGoogle();
    } else {
      account = await this.nativeGoogle();
    }

    return new Promise(resolve => resolve(account));
  }

  async logout() {
    await this.afAuth.signOut();
    try {
      await this.gplus.logout();
      await this.gplus.disconnect();
    } catch { }
  }
}
