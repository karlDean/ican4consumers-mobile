import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { StorageService } from './storage.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  public darkModeOn: boolean;

  private activatedMenu: string = 'home';
  private ToastElem: HTMLIonToastElement;
  private LoadingElem: HTMLIonLoadingElement;
  private AlertElem: HTMLIonAlertElement;

  constructor(
    private _toast: ToastController,
    private _loading: LoadingController,
    private _alert: AlertController,
    private storage: StorageService,
    private callNumber: CallNumber
  ) { }

  menu() {
    
    let _this = this;
    return ({
      selectMenu: function (menu: string) {
        _this.activatedMenu = menu;
      },
      activatedMenu: function () {
        return _this.activatedMenu;
      }
    })
  }

  async toast(message) {
    
    this.ToastElem = await this._toast.create({
      message: message,
      duration: 3000,
      keyboardClose: true,
      color: "dark"
    });

    return {
      present: () => this.ToastElem.present()
    }
  }

  dismissToast() {
    this.ToastElem.dismiss();
  }

  async loader(message = '') {

    this.LoadingElem = await this._loading.create({
      message: message
    });

    return {
      present: () => this.LoadingElem.present()
    }
  }

  dismissLoader() {
    this.LoadingElem.dismiss();
  }

  async alert(message: string, header: string = null) {
    this.AlertElem = await this._alert.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    return {
      present: () => this.AlertElem.present()
    }
  }

  async alertDidDismiss() {
    return await this.AlertElem.onDidDismiss()
  }

  dismissAlert() {
    this.AlertElem.dismiss();
  }

  async isDarkMode(): Promise<boolean> {
    let result: any = await this.storage.get('darkMode');
    return new Promise(resolve => resolve(result));
  }

  enableDarkMode(ON: boolean) {
    
    if (ON) {
      document.body.classList.toggle('onTransition', true);
      document.body.classList.toggle('dark', true);
      setTimeout(() => {
        document.body.classList.toggle('onTransition', false);
      }, 1001);
    } else {
      document.body.classList.toggle('onTransition', true);
      document.body.classList.toggle('light', true);
      document.body.classList.toggle('dark', false);
      setTimeout(() => {
        document.body.classList.toggle('light', false);
        document.body.classList.toggle('onTransition', false);
      }, 1001);
    }

    this.storage.set({ darkMode: ON });
  }

  dial(number) {
    this.callNumber.callNumber(number, true);
  }

}
