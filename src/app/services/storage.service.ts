import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  /**
   * Get key that exists in Storage.
   * 
   * @param key Key in Storage.
   */
  async get(key: string): Promise<string | object | string[] | boolean> {
    return new Promise(async resolve => {
      let value = (await Storage.get({ key: key })).value;

      try {
        value = JSON.parse(value);
      } catch {}

      if (value != null)
        resolve(value);
      else
        resolve(null);
    })
  }

  /**
   * Sets all keys and values in passed object.
   * 
   * @param args Object of keys and values.
   * 
   * Setting multiple keys - { key1: "value1", key2: "value2" }
   */
  async set({ ...data }: { [key: string]: string | object | number | boolean}): Promise<string> {
    return new Promise(async (resolve, reject) => {
      let keys = (await Storage.keys()).keys;
      let overwritten = [];
      for (let key in data) {
        keys.includes(key) ? overwritten.push(key) : null;
        let value = typeof data[key] == 'object' ? JSON.stringify(data[key]) : data[key].toString();
        Storage.set({ key: key, value: value }).catch(e =>reject(e));
      }
      resolve(`Success. ${overwritten.length ? overwritten.join(', ') + " is/are overwritten." : null}`);
    })    
  }

  /**
   * Remove keys and values in Storage.
   * @param keys Keys in Storage.
   */
  async remove(...keys: string[]) {
    return new Promise(async resolve => {
      let regKeys = (await Storage.keys()).keys;
      let unRemoved = [];
      for (let key of keys) {
        if (!regKeys.includes(key)) {
          unRemoved.push(key);
          continue;
        }

        await Storage.remove({ key: key });
      }

      unRemoved.length ? console.warn(`${unRemoved.join(', ') + " does not exist in Storage."}`) : null;
      resolve(true);
    })
  }

  /**
   * Wipe all Keys and Values in Storage.
   */
  async clear() {
    return new Promise(resolve => {
      Storage.clear()
        .then(e => resolve("Storage cleared."))
        .catch(e => resolve(e));
    })
  }

  async getAll() {
    return new Promise(resolve => {
      Storage.keys().
        then(async e => {
          let keys = e.keys;
          let stored = {};
          for (let k of keys) {
            let value = await this.get(k);
            stored[k] = value;
          }
          resolve(stored);
        });
    })
  }
}
