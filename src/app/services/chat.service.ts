import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { RequestService } from './request.service';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  // private chatAPI = 'http://localhost/AgentChat/client/';
  private chatAPI = 'https://chat.ican4consumers.com/client/';
  private typingSubscription: Subscription;
  private acceptingSubscription: Subscription;

  ReceiverWS: WebSocket;
  SenderWS: WebSocket;
  onReceive: BehaviorSubject<any> = new BehaviorSubject(null);
  requesting: BehaviorSubject<boolean> = new BehaviorSubject(false);
  client = {
    name: null,
    id: null
  };
  typing = false;
  chatEnded = false;
  agent: string;
  agentId;

  constructor(
    private storage: StorageService,
    private request: RequestService
  ) { }

  /**
   * 
   * Establish connection of sender and receiver to websocket
   * 
   */
  async EstablishConnection(): Promise<{sender: WebSocket | void, receiver: WebSocket | void}> {
    return new Promise(async (resolve, reject) => {

      let sender = await this.EstablishSender().catch(e => {
        reject(e);
      })

      let receiver = await this.EstablishReceiver().catch(e => {
        reject(e)
      })

      resolve({
        sender: sender,
        receiver: receiver
      });
    })
  }


  /**
   * 
   * Establish sender connection to websocket
   * 
   */
  private async EstablishSender(): Promise<WebSocket> {
    return new Promise((resolve, reject) => {

      if (this.SenderWS && this.SenderWS.readyState == WebSocket.OPEN) {
        return;
      }

      this.SenderWS = new WebSocket("wss://chat.ican4consumers.com:3486");

      this.SenderWS.onopen = () => {
        resolve(this.SenderWS);
      }

      this.SenderWS.onerror = (e) => {
        reject("error");
      }

    })
  }


  /**
   * 
   * Establish receiver connection to websocket
   * 
   */
  private async EstablishReceiver(): Promise<WebSocket> {
    return new Promise((resolve, reject) => {
      if (this.ReceiverWS && this.ReceiverWS.readyState == WebSocket.OPEN) {
        return;
      }

      this.ReceiverWS = new WebSocket("wss://chat.ican4consumers.com:3486");

      this.ReceiverWS.onopen = () => {
        resolve(this.ReceiverWS);
      }

      this.ReceiverWS.onerror = (e) => {
        reject("error");
      }

      this.ReceiverWS.onmessage = (e) => {
        this.onReceive.next(JSON.parse(atob(e.data)));
      }
    })
  }


  /**
   * 
   * Get chat history to chat API
   * 
   */
  async getHistory() {
    let client: any = await this.storage.get('user_info');
    if (client != 'guest') {
      this.client.name = `${client.firstname} ${client.lastname} - ICAN APP ${Math.random().toString(36).substr(2, 8)}`; //change to - ICAN App
      this.client.id = client.pre_login_id;
    } else {
      this.client.name = `guest ${Math.random().toString(36).substr(2, 8)}`; //change to - ICAN App
      this.client.id = Math.random().toString(36).substr(2, 8);
    }

    console.log(this.client);
    let result: any = await this.chatAPIRequest('fetch_convo', {
      client_id: this.client.id
    });

    return new Promise(resolve => resolve(result));
  }


  /**
   * 
   * API Call to chat API
   * @param action function in chat API
   * @param params data passed to chat API
   * 
   */
  async chatAPIRequest(action: string, params: object, method="get") {
    
    if (method == 'get') {
      let result = await this.request.get(this.chatAPI + action, {
        ...params
      });

      return new Promise(resolve => resolve(result));
    }

    if (method == "post") {
      let result = await this.request.post(this.chatAPI + action, {
        ...params
      });

      return new Promise(resolve => resolve(result));
    }

  }

  /**
   * 
   * Client requests for agent support
   * 
   */
  async requestAgent() {
    const data = {
      client_name: this.client.name,
      request_code: this.client.id,
      request_date: this.datetime(),
      request_from: 'ican',
      user_name: this.client.name,
      type: 'request',
      status: 'pending',
      pending_msg: ''
    }

    await this.insertRequest(data);

    this.requesting.next(true);
    this.SenderWS.send(btoa(JSON.stringify(data)));
    this.requestAccepted();

    return;

  }


  private requestAccepted(){
    this.acceptingSubscription = this.onReceive.subscribe(e => {

      if (!e)
        return;
      
      if (e.request_code != this.client.id)
        return;
      
      if (e.request_from != 'ican')
        return;

      if (e.user_id) {
        this.agent = e.user_name;
        this.agentId = e.user_id;
        this.requesting.next(false);
        this.requesting.complete();
        this.typingActivity();
        this.acceptingSubscription.unsubscribe();
      }
    });
    return;
  }

  private typingActivity() {
    this.typingSubscription = this.onReceive.subscribe(e => {
      if (!e.user_id)
        return;
      if (e.type == "Typing_true")
        this.typing = true;
      else if (e.type == "Typing_false")
        this.typing = false;
    });
  }

  
  /**
   * 
   * POst request to database
   * @param data request data
   * 
   */
  private async insertRequest(data: object){
    this.requesting.next(true);
    console.log("api insert request");
    console.log(
    await this.chatAPIRequest('insert_request', {
      ...data
    }));
    return;
  }


  private async removeRequest() {
    const data_socket = {
      request_code: this.client.id,
      client: this.client.name,
      request_status: 'disconnect',
      request_from: 'ican',
      request_date: this.datetime()
    }

    //remove request to api
    const data = {
      request_code: this.client.id,
      client: this.client.name,
      request_status: 'disconnect',
      request_from: 'ican',
      request_date: this.datetime()
    }
        
    this.SenderWS.send(btoa(JSON.stringify(data_socket)));
    await this.chatAPIRequest('remove_request', {
      reques_code: this.client.id
    });
    return;
  }


  /**
   * 
   * Client send message
   * 
   */
  sendMessage(message) {
    
    const data = {
      msg: message.replace(/^\s*(\n)\s*$/, ''),
      client_name: this.client.name,
      request_code: this.client.id,
      date_chat: this.datetime(),
      request_from: 'ican',
      user_id: '',
      user_name: '',
      type: 'send_message'
    }

    this.SenderWS.send(btoa(JSON.stringify(data)));

    return new Promise<{date: string, msg: string, name: string}>(resolve => {
      resolve({
        date: data.date_chat,
        msg: data.msg,
        name: data.client_name
      });
    });
  }


  /**
   * 
   * Trigger typing
   * @param state if typing or not
   * 
   */
  triggerTyping(state: boolean) {
    const data = {
      request_code: this.client.id,
      user_id: '',
      request_from: 'ican',
      type: 'Typing_' + state
    }
    this.SenderWS.send(btoa(JSON.stringify(data)));
  }

  async endChat() {
    await this.removeRequest();
    await this.insertChatLogs();
    try {
      this.typingSubscription.unsubscribe();
      this.acceptingSubscription.unsubscribe();
      this.SenderWS.close();
      this.ReceiverWS.close();
    } catch { }
    return;
  }

  
  private async insertChatLogs() {

    let new_chats_div = document.querySelector('.new-convo-bubbles');
    let history = new_chats_div.innerHTML;

    if (!history)
      return;

    let div_filter = document.createElement('div');
    div_filter.innerHTML = history;

    let imgDiv = div_filter.querySelectorAll('.agent_msg .img')

    for (let div in imgDiv) {
      if (typeof imgDiv[div] != "object")
        continue;
      let elem = imgDiv[div];
      elem.remove();
    }

    let clientImg = div_filter.querySelectorAll('.clients_msg img');
    
    for (let img in clientImg) {
      if (typeof clientImg[img] != "object")
        continue;
      
      clientImg[img].setAttribute('src', 'https://chat.ican4consumers.com/public/img/client.png');
    }

    let chat_history = div_filter.innerHTML;
    
    const data = {
      chat_history: btoa(chat_history),
      chat_time: this.datetime(),
      chat_room: this.client.name,
      client_id: this.client.id,
      agent_id: this.agentId
    }

    console.log(await this.chatAPIRequest('insert_chatlogs', data, 'post'));
    return;
  }


  /**
   * 
   * Get current datetime
   * 
   */
  datetime() {
    var d = new Date();
    var time = d.toLocaleTimeString().replace(/(.*)\D\d+/, '$1');
    var dateString: any = new Date();
    dateString = new Date(dateString).toUTCString();
    dateString = dateString.split(' ').slice(0, 4).join(' ');
    return dateString + ' ' + time;
  }
}
