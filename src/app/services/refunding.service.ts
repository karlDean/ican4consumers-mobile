import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { environment } from 'src/environments/environment';
import { AccountService } from './account.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class RefundingService {

  constructor(
    private request: RequestService,
    private account: AccountService,
    private storage: StorageService
  ) { }

  async cancelMembership(params: {
    descriptor: string,
    firstname: string,
    lastname: string,
    email_address: string,
    first6: string,
    last4: string,
    order_no: string
  }) {
    
    let result = await this.request.get(environment.refund_api,
      {
        request_type: 'cancelSubscription',
        api_key: environment.api_key,
        ...params
      });
    
    return new Promise(resolve => resolve(result));

  }

  async refund(refundDetails: {
    descriptor: string,
    refund_reason: string,
    first6: string,
    last4: string,
    purchase_date: string,
    amount: string,
    email_address: string,
    purchase_name: string,
    order_no?: string
    time?: string
  }) {

    refundDetails.purchase_date = refundDetails.purchase_date.split('T')[0];
    refundDetails.time = refundDetails.purchase_date.split('T')[0];

    let account = this.storage.get('user_info');

    if (!refundDetails.order_no)
      delete refundDetails.order_no;

    let params = {
      request_type: 'refund',
      api_key: environment.api_key,
      ...refundDetails,
      time: refundDetails.purchase_date,
      purchaser_name: refundDetails.purchase_name,
      pre_login_id: account['pre_login_id'] ? account['pre_login_id'] : 0
    }

    let result = await this.request.get(environment.refund_api, params);

    return new Promise(resolve => resolve(result));
    
  }

  async nmmRefund(data: any){
    let url = "http://www.ic4c.net/_api/api_non_member_merchant.php";
    let account = this.storage.get('user_info');
    let params = {
            'type' : 'float-refund-add',
            'apikey' : '!iw0rdpr3ss92315@@',
            'merchant-info': data.merch_info,
            'merchant-business': data.merch_business,
            'merchant-website': data.merch_website,
            'merchant-email': data.merch_email,
            'merchant-phone': data.merch_phone,
            'consumer-firstname': data.firstname,
            'consumer-lastname': data.lastname,
            'consumer-email': data.contact_email,
            'consumer-phone': data.contact_phone,
            'purchase-date': data.time.split('T')[0],
            'purchase-amount': data.amount,
            'purchase-email': data.purchaser_email,
            'purchase-reason': data.refund_reason,
            'purchase-product': data.product,
            'consumer-cardfirst' : data.first6,
            'consumer-cardlast' : data.last4,
            'consumer_id': account['pre_login_id']
    }

    console.log(params);
    let refund = await this.request.get(url, params) as any;
    console.log(refund);
    return new Promise((resolve, reject) => {
      if (refund.responsecode == 1)
        resolve(refund.responsestring);
      else
        reject(refund.responsestring);
    });
  }

  async getCardNetwork(card_bin) {
    
    return new Promise(async (resolve, reject) => {
      
      let card = await this.request.get(`https://lookup.binlist.net/${card_bin}`).catch(e => {
        reject(e);
      });

      resolve(card);
    })

  }

  async getRefundHistory(searchDetails: {
    searchKey: string,
    sort?: "amount" | "date" | "email",
    order?: "asc" | "desc"
  }): Promise<{ pendingRefund: any, processedRefund: any }> {
    

    let account = await this.storage.get('user_info');

    let params = {
      api_key: environment.api_key,
      request_type: 'getRefundHistory',
      token: account['mobile_token'],
    };

    searchDetails.searchKey ? params['search_key'] = searchDetails.searchKey : null;  
    searchDetails.sort ? params['sort'] = searchDetails.sort : null;  
    searchDetails.order ? params['order'] = searchDetails.order : null;  

    let result = await this.request.get(environment.refund_api, params);

    console.log(result);

    let history = result['responsemsg'] as Array<any>;

    let pendingRemarks = ["REFUND REQUESTED", "CANCELLATION REQUESTED"];

    let pendingRefund = history.filter(e => {
      return pendingRemarks.includes(e['refund_Status']);
    });
    
    let processedRefund = history.filter(e => {
      return !pendingRemarks.includes(e['refund_Status']);
    });

    return new Promise(resolve => resolve(
      {
        pendingRefund: pendingRefund,
        processedRefund: processedRefund
      }
    ));

    // return ;    
  }

  
}
