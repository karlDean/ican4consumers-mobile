import gsap from 'gsap';

export class ProductQuestionAnimation {

    entrance() {
        
        gsap.fromTo('.illustration h1 span', {
            translateX: '-180%',
            position: 'absolute'
        }, {
            translateX: 0,
            duration: 1.5,
            stagger: .05,
            ease: 'elastic.out(1, 0.5)'
        });

        gsap.fromTo('app-question-svg', {
            translateX: '100%'
        }, {
            translateX: '20%',
            duration: 2,
            ease: "elastic.out(1, 0.5)"
        });

        gsap.fromTo('.content > p', {
            translateY: 100,
            opacity: 0
        }, {
            translateY: 0,
            opacity: 1,
            duration: 1.5,
            ease: 'elastic.out(1, 0.5)'
        });

        gsap.fromTo('.option-item', {
            translateY: 100,
            opacity: 0,
        }, {
            translateY: 0,
            opacity: 1,
            duration: 1.5,
            stagger: .2,
            delay: .2,
            ease: 'elastic.out(1, 0.5)'
        }).then(_ => {
            
            gsap.to('.option-item', {
                transition: '.4s'
            })
        })

    }

}
