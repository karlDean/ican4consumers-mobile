import gsap from 'gsap';

export class MenuAnimation {

    entrance() {
        
        gsap.fromTo('.menu-item', {
            translateX: '-120%'
        }, {
            translateX: 0,
            duration: 1.5,
            delay: .1,
            ease: 'elastic.out(1, .8)',
            stagger: .05
        });

        gsap.fromTo('.menu-extendee', {
            translateX: '-100%',
            opacity: 0
        }, {
            translateX: 0,
            duration: .4,
            opacity: 1
        });

        gsap.fromTo('.custom-header .container ion-button', {
            pointerEvents: 'initial'
        }, {
            pointerEvents: 'none'

        });

    }

    exit() {
        gsap.fromTo('.menu-extendee', {
            translateX: 0,
            opacity: 1
        }, {
            translateX: '-100%',
            duration: .4,
            opacity: 0
        });

        gsap.fromTo('.custom-header .container ion-button', {
            pointerEvents: 'none'
        }, {
            pointerEvents: 'initial'
        });
    }

}
