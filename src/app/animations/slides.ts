import { gsap } from 'gsap';

export class Slides {


    slideAnim(slide: any) {
        slide = slide.el;
        const h1 = slide.querySelector('h1');
        const buttons = slide.querySelector('.slide-buttons');

        gsap.fromTo(h1, {
            translateY: 50,
            opacity: 0,
        }, {
            translateY: 0,
            opacity: 1,
            duration: .4,
            delay: .2
        })

        gsap.fromTo(buttons, {
            translateY: 50,
            opacity: 0,
        }, {
            translateY: 0,
            opacity: 1,
            duration: .4,
            delay: .6
        })
    }

    slideLastAnim(slide) {
        slide = slide.el;
        const h3 = slide.querySelector('h3');
        const h1 = slide.querySelector('h1');
        const span = h1.querySelector('span');
        const h2 = slide.querySelector('h2');
        const button = slide.querySelector('ion-button');
        const slideButtons = slide.querySelector('.slide-buttons');

        gsap.fromTo(h3, {
            translateY: 50,
            opacity: 0
        }, {
            translateY: 0,
            opacity: 1,
            delay: .2,
            duration: .6
        });

        gsap.fromTo(h1, {
            opacity: 0,
            scale: .8
        }, {
            opacity: 1,
            scale: 1,
            delay: .4,
            duration: 1
        });

        gsap.fromTo(span, {
            opacity: 0,
            scale: .8
        }, {
            opacity: 1,
            scale: 1,
            delay: .8,
            duration: 1
        });

        gsap.fromTo(h2, {
            opacity: 0,
            translateY: -50
        }, {
            opacity: 1,
            translateY: 0,
            delay: 1.2,
            duration: .6
        });

        gsap.fromTo(button, {
            opacity: 0
        }, {
            opacity: 1,
            duration: .6,
            delay: 1.6
        });

        gsap.fromTo(slideButtons, {
            translateY: 50,
            opacity: 0,
        }, {
            translateY: 0,
            opacity: 1,
            duration: .4,
            delay: 1.8
        })
    }
}
