import gsap from 'gsap';

export class Home_Animation {

    header_footer_entrance() {

        gsap.fromTo('app-custom-header', {
            translateY: '-100%'
        }, {
            translateY: 0,
            duration: .8
        });
        gsap.fromTo('app-custom-footer', {
            translateY: '100%'
        }, {
            translateY: 0,
            duration: .6
        });

    }

    main() {
        
        gsap.fromTo('.support h1 span', {
            translateX: '-150%',
            position: 'absolute'
        }, {
            translateX: 0,
            duration: 1.5,
            stagger: .2,
            ease: "elastic.out(1, 0.5)"
        });

        gsap.fromTo('app-support-svg', {
            translateX: '100%'
        }, {
                translateX: '20%',
            duration: 1.5,
            ease: "elastic.out(1, 0.5)"
        });

        gsap.fromTo('.help-item', {
            translateY: '100%',
            opacity: 0
        }, {
            translateY: 0,
            opacity: 1,
            stagger: .2,
            duration: 1.5,
            ease: "elastic.out(1, 0.5)"
        });

    }

}
