import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController, createAnimation, ModalController } from '@ionic/angular';
import { Router, NavigationEnd } from '@angular/router';
import { StorageService } from './services/storage.service';
import { MenuAnimation } from './animations/menu-animation';
import { TxnPickerComponent } from './components/txn-picker/txn-picker.component';
import { UiService } from './services/ui.service';
import { Subscription } from 'rxjs';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { SplashScreen } from '@capacitor/splash-screen';
import gsap from 'gsap';
import { AccountService } from './services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  @ViewChild('afterSplash') afterSplash;

  anim = new MenuAnimation;
  public account;

  constructor(
    private platform: Platform,
    private router: Router,
    private menu: MenuController,
    private storage: StorageService,
    private accountService: AccountService,
    private modal: ModalController,
    private nav: NavController,
    private ui: UiService,
    private appMin: AppMinimize,
  ) { 
    this.initializeApp();
  }
  
  initializeApp() {
    this.platform.ready().then(async () => {

      this.accountService.accountChanged.subscribe(e => {
        this.account = e;
      });

      this.account = await this.storage.get('user_info');
      
      let darkMode = await this.storage.get('darkMode');
      
      if (darkMode == null) {
        document.body.classList.toggle('dark', false);
        this.ui.darkModeOn = false;
      } else if (darkMode == true) {
        this.ui.darkModeOn = true;
        document.body.classList.toggle('dark', true);
      } else {
        this.ui.darkModeOn = false;
        document.body.classList.toggle('dark', false);
      }

      setTimeout(() => {
        SplashScreen.hide();

        let vid = this.afterSplash.nativeElement;
        gsap.fromTo(vid, {
          opacity: 0
        }, {
          opacity: 1
        });
        vid.play();
        vid.muted = true;
        setTimeout(() => {
          gsap.fromTo(vid, {
            opacity: 1
          }, {
            opacity: 0
          });
        }, 6000);
        setTimeout(() => {
          this.final();
        }, 6500);
      }, 500);      
    })
  }

  async final() {
    this.router.navigate(['intro']);
    this.menu.swipeGesture(false);

    window.addEventListener('keyboardWillHide', () => {
      const app_chatroom: any = document.querySelector('app-chatroom');
      window.requestAnimationFrame(() => {
        app_chatroom.style.height = '100%';
        window.requestAnimationFrame(() => {
          app_chatroom.style.height = '';
        });
      });

      const app_login: any = document.querySelector('app-login');
      window.requestAnimationFrame(() => {
        app_login.style.height = '100%';
        window.requestAnimationFrame(() => {
          app_login.style.height = '';
        });
      });
    });

    let backButton: Subscription;
  

    let backButtonDisabled = (e) => {
      e.preventDefault();
      e.stopPropagation();
    }

    let minimizeFunc = () => {
      this.appMin.minimize();
    }

    let navBackFunc = () => {
      this.nav.back();
    }

    let homeBackFunc = () => {
      document.addEventListener('backbutton', backButtonDisabled, false);
      this.router.navigate(['home']);
    }

    let thankBackFunc = () => {
      document.addEventListener('backbutton', backButtonDisabled, false);
      this.router.navigate(['thank-you']);
    }

    this.router.events.subscribe(e => {
      if (!(e instanceof NavigationEnd))
        return;
    
      let url = e.url.replace(/^\/|\/$/, '').trim();

      switch (url) {
        case 'home':
          backButton = this.platform.backButton.subscribeWithPriority(99, minimizeFunc);
          break;
      
        case 'track-result':
          backButton = this.platform.backButton.subscribeWithPriority(99, navBackFunc);
          break;
      
        case 'thank-you':
          backButton = this.platform.backButton.subscribeWithPriority(99, homeBackFunc);
          break;
      
        case 'cancellation-result':
          backButton = this.platform.backButton.subscribeWithPriority(99, thankBackFunc);
          break;
      
        case 'refund/result':
          backButton = this.platform.backButton.subscribeWithPriority(99, thankBackFunc);
          break;
    
        default:
          if (backButton)
            backButton.unsubscribe();
          document.removeEventListener('backbutton', backButtonDisabled, false);
          break;
      }
    })
  }


  menuOpen() {
    this.anim.entrance();
  }

  menuClose() {
    this.anim.exit();
  }

  navigate(url) {
    this.menu.close();
    setTimeout(async () => {
      await this.router.navigate([url]);
      if (url == 'home') {
        let home_btn: HTMLIonSlidesElement = document.querySelector('app-custom-footer #home');
        home_btn.click();
        // home_slide.lockSwipes(false);
        // home_slide.slideTo(0);
        // home_slide.lockSwipes(true);
      }
    }, 100);
  }

  activatedRoute(route) {
    if (this.router.url.replace(/(^\/)|(\/$)/g, "") == route)
      return 'active';
  }
}