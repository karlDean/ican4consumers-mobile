// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_key: 'MOBA97!HX1@202!',
  refund_api: 'https://ican4consumers.com/refund/api'
};

export const firebaseConfig = {
    apiKey: "AIzaSyCV7KUMlnzH3h0Au0szndYGGOgPFAJCeh8",
    authDomain: "ican-android-6966b.firebaseapp.com",
    projectId: "ican-android-6966b",
    storageBucket: "ican-android-6966b.appspot.com",
    messagingSenderId: "511599695525",
    appId: "1:511599695525:web:d9adff0724ba4c09d89f4c",
    measurementId: "G-1BEE7JX4CJ"
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
